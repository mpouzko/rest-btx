<?php

function AddOrder($params){
	global $USER;
	//$siteId = 's1'; // код сайта
	//$userId = $USER->GetID(); // ID пользователя
	//print_r($userId);
	//die();
	if (!$USER->IsAuthorized() && !$_REQUEST[DBG]) 
		return ['error' =>"Вы не авторизованы!"];
	if($_REQUEST[DBG] == 1 || $_REQUEST[DBG] == 2 )	{	
		$params = array(
		  "productId" => 10449,
		  "categoryId" => 8,
		  "dateIn" => "2020-01-01",
		  "dateOut" => "2020-01-10",
		  "adults" => 2,
		  "children" => 0,
		  "time" => "10:10:10",
		  "Fname" => 'Василий',
		  "Lname" => 'Пупкинс',
		  "email" => 'email@email.ru',
		);
		//$userId = 1;
		$USER->Authorize(11);
		
		//$ret = $USER->Login($params['login'], $params['password'], "Y", "Y");
	}

	$userId = $USER->GetID();


  $fuserId = \Bitrix\Sale\Fuser::getId(false);
  //print_r($fuserId);
  //die();
  $basket = \Bitrix\Sale\Basket::loadItemsForFUser($fuserId, LANG); 
  //print_r([$userId, LANG]);
  //print_r($basket);
  //echo $basket->count();
  //die();
  if($basket->count() == 0)
    return ['error' =>"Корзина пуста"];  
    //die();
	$order = \Bitrix\Sale\Order::create(LANG, $userId);

  foreach ($order->getPropertyCollection() as $property) {
    if ($property->getField('CODE') === 'FIO') {
        $property->setValue($USER->GetFullName());
    }
    if ($property->getField('CODE') === 'EMAIL') {
      $property->setValue($USER->GetEmail());
    }
    //Не получаем телефон, надо смотреть
    if ($property->getField('CODE') === 'PHONE') {
      $property->setValue($USER->GetParam("EMAIL"));
    }

  }
	$order->setPersonTypeId(1); // ID типа плательщика 1 - Физ лицо
  $order->setField('USER_DESCRIPTION', 'Комментарий к заказу');  // Указываем если требуется
	$order->setBasket($basket);
	$paymentCollection = $order->getPaymentCollection();
	$payment = $paymentCollection->createItem(
		$z = Bitrix\Sale\PaySystem\Manager::getObjectById(TINKOFF_PAY_ID) // 1 - ID платежной системы
	);
	$payment->setField("SUM", $order->getPrice());
  $payment->setField("CURRENCY", $order->getCurrency());
  $payment->setField("ACCOUNT_NUMBER", $order->getUserId());
	$r = $order->save();
	if (!$r->isSuccess())
	{ 
		return $r->getErrorMessages();
		
  }
  $OrderId = $r->getId();
  $service = \Bitrix\Sale\PaySystem\Manager::getObjectById($payment->getPaymentSystemId());
  $context = \Bitrix\Main\Application::getInstance()->getContext();
  $service->initiatePay($payment, $context->getRequest());
  if(array_key_exists('payURL', $_SESSION)){
    $rt = $_SESSION['payURL'];
    unset ($_SESSION['payURL']);
    return ['payURL' => $rt];
  }
  else
    return [ 'error' => "can't get pay url"];
}

?>