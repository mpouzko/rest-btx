<?php
include (dirname(__FILE__).'/GetCart.php');
//include ("./GetCart.php");
function AddToCart($params){
	
	if($_REQUEST[DBG] == 1 || $_REQUEST[DBG] == 2 || $params['test'])	{	
		$params = array(
		  "productId" => 10449,
		  "categoryId" => 8,
		  "dateIn" => "2020-01-01",
		  "dateOut" => "2020-01-10",
		  "adults" => 2,
		  "children" => 0,
		  "time" => "10:10:10",
		  "Fname" => 'Василий',
		  "Lname" => 'Пупкинс',
		  "email" => 'email@email.ru',
		);
  }
  
  $Ent = new ENTITY($params['categoryId']);
  $Item = $Ent->GetItemPropByID($params['productId']);
  $summ = 0;
  if($params['categoryId'] == BOOKING){
    
    $dateIn = new datetime($params['dateIn']);
    $dateOut = new datetime($params['dateOut']);
    for ($i = $dateIn; $i <= $dateOut; date_add($dateIn, date_interval_create_from_date_string('1 days'))) {
      //$price[] = [getPrice($arItem['ID'], $params, $dateIn->format('Y-m-d')), $dateIn->format('Y-m-d'), $params];
      $summ += (int)getPrice($params['productId'], $params, $dateIn->format('Y-m-d'));
    }  
  }
  else
    $summ += (int)getPrice($params['productId'], $params);
	
  $arFields = array(
    "PRODUCT_ID" => $params['productId'],
    "PRODUCT_PRICE_ID" => 0,
    "PRICE" => $summ,//getPrice($params['productId'], $params, $params['dateIn']),
    "CURRENCY" => "RUB",
    "QUANTITY" => 1,
    "LID" => LANG,
    "DELAY" => "N", //Отложен товар
    "CAN_BUY" => "Y",
    "NAME" => $Item[0]['name'],
    "MODULE" => "catalog",
	  "NOTES" => "",
    "DETAIL_PAGE_URL" => "/personal/cart/"
  );
  $arProps = array();

  $arProps[] = array(
    "NAME" => "Дата заезда",
    "CODE" => "dateIn",
    "VALUE" => $params["dateIn"]
  );

  $arProps[] = array(
    "NAME" => "Дата выезда",
    "CODE" => "dateOut",
    "VALUE" => $params["dateOut"]
  );
  $arProps[] = array(
    "NAME" => "Взрослых",
    "CODE" => "adults",
    "VALUE" => $params["adults"]
  );  
  $arProps[] = array(
    "NAME" => "Детей",
    "CODE" => "children",
    "VALUE" => $params["children"]
  );
  $arProps[] = array(
    "NAME" => "Время",
    "CODE" => "time",
    "VALUE" => $params["time"]
  );
  $arProps[] = array(
    "NAME" => "Имя",
    "CODE" => "Fname",
    "VALUE" => $params["Fname"]
  );
  $arProps[] = array(
    "NAME" => "Фамилия",
    "CODE" => "Lname",
    "VALUE" => $params["Lname"]
  );
  $arProps[] = array(
    "NAME" => "email",
    "CODE" => "email",
    "VALUE" => $params["email"]
  );  
  
  
  //$arProp[] = AddContactData($arProp);
  $arFields["PROPS"] = $arProps;

  $result = array(
    'itemCardId' => CSaleBasket::Add($arFields)
  );
  return GetCart($params);
  //return $result;
}
function AddContactData($arProp){
	$arProp[] =
		array(	'code' => 'Fname',
				'value'=> $ar_res['VALUE'],
				'title'=> $ar_res['NAME'],
				'required'=> false,
				'edit' => true
			);
	$arProp[] =
		array(	'code' => 'Lname',
				'value'=> $ar_res['VALUE'],
				'title'=> $ar_res['NAME'],
				'required'=> false,
				'edit' => true
			);
	$arProp[] =
		array(	'code' => 'email',
				'value'=> $ar_res['VALUE'],
				'title'=> $ar_res['NAME'],
				'required'=> false,
				'edit' => true
			);
}
?>