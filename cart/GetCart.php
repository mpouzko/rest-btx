<?php
function GetCart($params){
	
	if($_REQUEST[DBG] == 1 || $_REQUEST[DBG] == 2 )	{	
		$params = array(
		);
	}	
	
	$arBasketItems = array();
	$dbBasketItems = CSaleBasket::GetList(
			array(
					"NAME" => "ASC",
					"ID" => "ASC"
					),
			array(
					"FUSER_ID" => CSaleBasket::GetBasketUserID(),
					"LID" => SITE_ID,
					"ORDER_ID" => "NULL"
					),
			false,
			false,
			array("*")
			);	
			
			
	while ($arItems = $dbBasketItems->Fetch())
	{
		if (strlen($arItems["CALLBACK_FUNC"]) > 0)
		{
			CSaleBasket::UpdatePrice($arItems["ID"], 
									 $arItems["CALLBACK_FUNC"], 
									 $arItems["MODULE"], 
									 $arItems["PRODUCT_ID"], 
									 $arItems["QUANTITY"]);
			$arItems = CSaleBasket::GetByID($arItems["ID"]);
		}

		//$arBasketItems[] = $arItems;
		$db_res = CSaleBasket::GetPropsList(
				array(
						"SORT" => "ASC",
						"NAME" => "ASC"
					),
				array("BASKET_ID" => $arItems['ID'])
			);
		$arProp = [];
		while ($ar_res = $db_res->Fetch())
		{
		   //echo $ar_res["NAME"]."=".$ar_res["VALUE"]."<br>";
		   $arProp[] = array('code' => $ar_res['CODE'],
							'value'=> $ar_res['VALUE'],
							'title'=> $ar_res['NAME'],
							'required'=> IsRequired($ar_res['CODE'], $params['categoryId']),
							//'required'=> 7,
							'edit' => IsEdit($ar_res['CODE']),
							);

		}		
		//$arProp = contactdata($arProp);
		
		$arBasketItems[] = array(
			'id' => $arItems['ID'],
			'productid' => $arItems['PRODUCT_ID'],
			'partner' => GetPartnerByProductId($arItems['PRODUCT_ID']),
			'name' => $arItems['NAME'],
			'price' => (int)$arItems['PRICE'],
			'quantity' => $arItems['QUANTITY'],
			'props' => $arProp,
			'test' => $arItems
		);		
	}	
	return $arBasketItems;
}

function IsRequired($code, $CategoryId = 7){
	$ret = false;
	switch($code){
		case 'adults':
			$ret = true;
		break;
		case 'dateOut':
			if($CategoryId != 7)
				$ret = true;
		break;
		case 'dateIn':
			$ret = true;
		break;
		case 'children':
			$ret = true;
		break;	
		case 'time':
			if($CategoryId == 7)
				$ret = true;
		break;	
		default:
			$ret = false;
		break;
	}
	return $ret;
}
function IsEdit($code){
	$ret = false;
	switch($code){
		case 'Fname':
				$ret = true;
		break;	
		case 'Lname':
				$ret = true;
		break;	
		case 'email':
				$ret = true;
		break;	
		default:
			$ret = false;
		break;		
	}
	return $ret;
	
}
function contactdata($arProp){
	$arProp[] =
		array(	'code' => 'Fname',
				'value'=> $ar_res['VALUE'],
				'title'=> $ar_res['NAME'],
				'required'=> false,
				'edit' => true
			);
	$arProp[] =
		array(	'code' => 'Lname',
				'value'=> $ar_res['VALUE'],
				'title'=> $ar_res['NAME'],
				'required'=> false,
				'edit' => true
			);
	$arProp[] =
		array(	'code' => 'email',
				'value'=> $ar_res['VALUE'],
				'title'=> $ar_res['NAME'],
				'required'=> false,
				'edit' => true
			);
}
function GetPartnerByProductId($ID){
	$ret = false;
	$arSelect = Array("IBLOCK_ID"); //PROPERTY_CML2_LINK //"PROPERTY_124"  , "PROPERTY_CML2_LINK", "PROPERTY_124", "PROPERTY_124_LINK"
	$arFilter = Array("ID" => $ID); //, "ACTIVE"=>"Y"	
	$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect); //CML2_LINK
	while($ob = $res->GetNextElement()){
		$arFields = $ob->GetFields();
		$IBLOCK_ID = $arFields['IBLOCK_ID'];
	}
	unset($res);
	$arSelect = Array("ID", "NAME", "IBLOCK_ID", "IBLOCK_SECTION_ID", "PROPERTY_CML2_LINK"); //PROPERTY_CML2_LINK //"PROPERTY_124"  , "PROPERTY_CML2_LINK", "PROPERTY_124", "PROPERTY_124_LINK"
	$arFilter = Array("ID" => $ID, "IBLOCK_ID" => $IBLOCK_ID); //, "ACTIVE"=>"Y"	
	



	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect); //CML2_LINK
	while($ob = $res->GetNextElement())
	{
		$arFields = $ob->GetFields();
		if($arFields['IBLOCK_ID'] == BOOKING_ROOMS)
			$arFilterP = Array("PROPERTY_products"=>$arFields['IBLOCK_SECTION_ID']); //, "ACTIVE"=>"Y"
		else
			$arFilterP = Array("ID"=>$arFields['PROPERTY_CML2_LINK_VALUE']); //, "ACTIVE"=>"Y"	
		$arSelectP = Array("ID", "NAME", "IBLOCK_SECTION_ID", "IBLOCK_ID", 'CODE');
		$partner = CIBlockElement::GetList(Array(), $arFilterP, false, Array("nPageSize"=>1), $arSelectP);
		$obP = $partner->GetNextElement();
		$arPFields = $obP->GetFields();

		$dbProperty = \CIBlockElement::getProperty($arPFields['IBLOCK_ID'], $arPFields['ID'], array("sort", "asc"), array('CODE' => 'photos'));
		$Field = $dbProperty->GetNext();
		//while ($arProperty = $dbProperty->GetNext()) {
			$img_path_small = CFile::ResizeImageGet($Field['VALUE'], array('width'=>360, 'height'=>360), BX_RESIZE_IMAGE_PROPORTIONAL, true);
			$img_path_mid = CFile::ResizeImageGet($Field['VALUE'], array('width'=>720, 'height'=>720), BX_RESIZE_IMAGE_PROPORTIONAL, true);
			$arPhoto['small'] = SERVER_NAME_HTTP.$img_path_small['src'];
			$arPhoto['mid'] = SERVER_NAME_HTTP.$img_path_mid['src'];
			$Photo[] = $arPhoto;
		
			//$Photo[]= $arProperty;
		//}

/*
			$img_path_small = CFile::ResizeImageGet($Field['VALUE'], array('width'=>360, 'height'=>360), BX_RESIZE_IMAGE_PROPORTIONAL, true);
			$img_path_mid = CFile::ResizeImageGet($Field['VALUE'], array('width'=>720, 'height'=>720), BX_RESIZE_IMAGE_PROPORTIONAL, true);
			$arPhoto['small'] = SERVER_NAME_HTTP.$img_path_small['src'];
			$arPhoto['mid'] = SERVER_NAME_HTTP.$img_path_mid['src'];
			$result = $arPhoto;
*/


		$ret[] = array(
			'id' => $arPFields['ID'],
			'name' => $arPFields['NAME'],
			'code' => $arPFields['CODE'],
			'categoryId' => $arPFields['IBLOCK_ID'],
			//'categoryCode' => $arPFields['IBLOCK_CODE'],
			'subcategoryId' => $arPFields['IBLOCK_SECTION_ID'],
			'photo' => $Photo
		);
	}
return $ret;
	
}

?>