<?

function QuickOrder($params){
    GLOBAL $USER;
    //if($USER->IsAuthorized()){
        //$USER->Logout();
    //}

    if($_REQUEST[DBG] == 1 || $_REQUEST[DBG] == 2 )	{	
        $params = array(
            "fname" => "test",
            "lname" => "test",
            //"password" => 'testtest2',
            //"password_confirm" => 'testtest2',
            "email" => '12123112@123.ru',
            "phone" => "123456789",
            'feedback' => true,
            'createAccount' => false
        );
    }
    if($params['createAccount'] == false){
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $params['password'] = 'password-'.substr(str_shuffle($permitted_chars), 0, 16);
        $params['password_confirm'] = $params['password'];
    }
    //LogData("Пароль", $params, DBG);    
    if(!$params['phone'] || !$params['password'] || !$params['email'] || !$params['password_confirm']){
        //header('HTTP/1.0 403 Forbidden');
        return ['msg' => 'не указаны обязательные параметры', 'error' => 403];        
    }

    $rsUser = CUser::GetByLogin($params['email']);
    if($arUser = $rsUser->Fetch()){
        return ['msg' => 'такой email уже существует', 'error' => 4403];  
    } else {
        $arResult = $USER->Register($params['email'], $params['fname'], $params['lname'], $params['password'], $params['password_confirm'], $params['email'], LANG);
        if($arResult['TYPE'] == 'ERROR')
            return ['msg' => strip_tags($arResult['MESSAGE']), 'error' => 53453];
        $USER->Update($USER->GetID(), ["PERSONAL_MOBILE" => $params['phone']]);

        $USER->IsAuthorized($USER->GetID());

        include_once (dirname(__FILE__).'/AddOrder.php');
        $return = AddOrder($params);
        //LogData("Пароль2", $return, DBG);  
    }

    if($USER->IsAuthorized()){
        $USER->Logout();
    }
    return $return;
}
    
?>