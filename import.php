<?php
header('Content-Encoding: UTF-8');
include (dirname(__FILE__).'/includes/main.php');
include (dirname(__FILE__).'/includes/import.php');

define(SHOW, false);
define(IBLOCKROOM, 8);

//$data['hotelname'] = "Марриотт Сочи Красная Поляна";
//addSku($data);
die();
//echo "<pre>";
$files = scandir ("./prices/import");
//print_r($files);
foreach($files as $file){
	
	if($file == "." or $file == ".."){
		//echo "<br>";
		//print_r($file);	
	}
	else{
		//echo "<pre>";
		//print_r($file);	
		processFile($file);
		break;
	}
}



function processFile($file){
	$filename = dirname(__FILE__)."/prices/import/$file";	
	//echo $filename;
	$handle = fopen($filename, "r");
	if(!$handle)
		echo "error";
	$i = 0;

	$cntLine = 0;
	$cntFoodDate = 0;
	$cntRoom = 0;
	$cntHR = 0;
	$hotelname = "";
	$roomname = "";
	$guests = "";

	$room = array();

	$result = array();
	$x = 0;
	$res = [];
	while($contents = fgetcsv($handle, 0, ";")){
		//--------Определяем отель
		if($cntLine == 0){
			$hotelname = $contents[0];
			//echo cnv($hotelname);
			$cntLine++;
			if($cntHR == 2 && $contents[0] == ""){
				goto a;
			}		
			$cntHR = 0;	
		}
		else{
			//---------Определяем что за строка
			if($cntFoodDate == 0){
				//---------Тип питания и даты
				$res = GetFoodAndDatе($contents);
				$food = $res['food'];
				//echo $res['food'];
				//$res['fullperiod'] = GetFullFoodAndDatе($contents)['period'];
				$cntFoodDate++;
			}
			else{
				//---------Название номера
				if($cntRoom == 0){
					$roomname = $contents[0];
					//$res['roomname'] = cnv($roomname);
					$cntRoom++;
				}
				else{
					//---------цены, гости и пробелы
					if($contents[0] != "" && $contents[1] != ""){
						//---------Обрабатываем гостей и цены
						$guests = $contents[0];
						$res['guests'][] = $guests;
						for($i = 0; $i <= count($contents)-2; $i++){
							//$res['price'][$i][] = $contents[$i+1];//array('price' => $contents[$i], 'guests' => $contents[0]);
							//$res['test'][] = array('guests' => $guests, 'period' => $res['period'][$i], 'price' => $contents[$i+1], 'roomname' => cnv($roomname), 'hotelname' => cnv($hotelname), 'food' => $food);//array('price' => $contents[$i], 'guests' => $contents[0]);
							$res['tmp'][] = array('guests' => $guests, 'startdate' => $res['period'][$i]['startdate'], 'enddate' => $res['period'][$i]['enddate'], 'price' => $contents[$i+1], 'roomname' => cnv($roomname), 'hotelname' => cnv($hotelname), 'food' => $food);//array('price' => $contents[$i], 'guests' => $contents[0]);
						}
					}
					elseif($cntHR == 1){
						//---------Тут мы нашли второй пробел
						$cntLine = 0;
						$cntFoodDate = 0;
						$cntRoom = 0;
						$cntHR++;	
						$artmp = $room;
						$room = array_merge($artmp, $res['tmp']);
					}
					else{
						//---------Тут мы нашли первый пробел
						$cntHR++;
					}
				}
				
			}
		}		
	}
	a:
	//print_r($res['tmp']);
	$room = array_merge($room, $res['tmp']);
	//print_r(json_encode($room));
	//print_r($room);
	fclose($handle);
	
	//print_r($room);
	//die();
	addSku($room);	
	//die();
}

function addSku($rooms){
	$x=0;
	$Ent = new ENTITY(7);
	//print_r($rooms);
	//die();
	$hotel = $Ent->GetItemByPriceName($rooms[0]['hotelname']);	
	foreach ($rooms as $room){
		//print_r($room);
		$x++;
		if(SHOW){
			addSkuToItem($hotel, $room);
			die();
		}
		else {
			//if($x < 100)
				addSkuToItem($hotel, $room);
		}
		
	}
	echo "<br>Записано $x строк<br>";



	//price_name
	/*
	{
		"guests": "1",
		"startdate": "2019-12-30 00:00:00",
		"enddate": "2019-12-31 23:59:59",
		"price": "28600",
		"roomname": "Делюкс с одной большой кроватью",
		"hotelname": "Марриотт Сочи Красная Поляна",
		"food": "BB"
	}
	*/
	//print_r(json_encode($arItems));
}

function addSkuToItem($hotel, $sku){
	print_r($hotel);
	print_r($sku);
	$room = checkRoom($hotel, $sku);
	//print_r($room);
	if($room){
		$ciBlockElement = new CIBlockElement;
		$format = "YYYY.MM.DD HH:MI:SS";
		$newformat = "DD.MM.YYYY";
		//$startdate = explode(" ", $sku['startdate']);
		//$enddate = explode(" ", $sku['enddate']);
		//print_r(date("d.m.Y", strtotime($sku['startdate'])));
		//die();
		// добавляем нужное кол-во торговых предложений
		//$adch = LookupSku('adults', $sku['guests']);
		$arLoadProductArray = array(
			"IBLOCK_ID"      => 10, // IBLOCK торговых предложений
			"NAME"           => $hotel['id'],//$sku['roomname']." ". date("d.m.Y", strtotime($sku['startdate']))." ". $sku['guests'],
			"ACTIVE"         => "Y",
			'PROPERTY_VALUES' => array(
				'CML2_LINK' => $room['id'], // Свойство типа "Привязка к товарам (SKU)", связываем торг. предложение с товаром
				'food' => LookupSku('food', $sku['food']), // тип питания food
				'adults' => LookupSku('adults', $sku['guests']), // тип питания food
				'children' => LookupSku('children', $sku['guests']), // тип питания food
				'startdate' => date("d.m.Y", strtotime($sku['startdate'])), // тип питания food
				'enddate' => date("d.m.Y", strtotime($sku['enddate'])),//$enddate[0], // тип питания food
				'reff' => 56,//$enddate[0], // тип питания food
				'guests' => $sku['guests'],
				'hotelid' => $hotel['id']
				//'test' => $sku['food'], // тип питания food
			)
			// Прочие параметры товара 
		);
		//echo "<br>-----------------<br>";
		//print_r($arLoadProductArray);
		//echo "<br>-----------------<br>";
		//print_r($startdate);
		$product_offer_id = $ciBlockElement->Add($arLoadProductArray);
		// проверка на ошибки
		if (!empty($ciBlockElement->LAST_ERROR)) {
			echo "Ошибка добавления торгового предложения: ". $ciBlockElement->LAST_ERROR;
			die();
		}
		// Добавляем параметры к торг. предложению
		CCatalogProduct::Add(
			array(
				"ID" => $product_offer_id,
				"QUANTITY" => 100
			)
		);
		// Добавляем цены к торг. предложению
		CPrice::Add(
			array(
				"CURRENCY" => "RUB",
				"PRICE" => $sku['price'],
				"CATALOG_GROUP_ID" => 1,
				"PRODUCT_ID" => $product_offer_id,
			)
		);
		
	}
	
	
	
	//die();
}

function LookupSku($code, $value){
	switch ($code){
		case "adults":
			//echo "<br>adults<br>";
			$ret = ParceGuests($value);
			//print_r($ret);
			switch($ret['adults']){
				case "1":
					return 45;
				break;	
				case "2":
					return 46;	
				break;	
				case "3":
					return 47;
				break;	
				case "4":
					return 48;
				break;					
				case "5":
					return 58;
				break;					
				case "6":
					return 59;
				break;					
				case "7":
					return 60;
				break;					
				case "8":
					return 61;
				break;					
			}
		break;
		case "children":
			//echo "<br>children<br>";
			$ret = ParceGuests($value);
			//print_r($ret);
			switch($ret['childs']){
				case "0":
					return 63;
				break;	
				case "1":
					return 49;
				break;	
				case "2":
					return 50;	
				break;	
				case "3":
					return 51;
				break;	
				case "4":
					return 62;
				break;	
				default:
					return 63;
				break;
			}
		break;
		case "food":
			switch($value){
				case "BB":
					return "52";
				break;	
				case "HB":
					return "53";
				break;	
				case "RO":
					return "54";
				break;	
			}
		break;
	}
}

function checkRoom($hotel, $sku){
	
		$dbItem = \Bitrix\Iblock\ElementTable::getList(array(
			'select' => array('ID', 'IBLOCK_ID', 'NAME', 'IBLOCK_SECTION_ID', 'CODE'),
			'filter' => array('IBLOCK_ID' => 8, 'NAME' => $sku['roomname'], 'IBLOCK_SECTION_ID' => $hotel['catalogid']),
			//'limit' => PAGE_LIMIT,
			//'offset' => $offset,			
			//'order' => array('TIMESTAMP_X' => 'ASC')
		));	
		$rooms = $dbItem->fetchAll();
		
		if(count($rooms) > 1){
			print_r("Много номеров ".count($rooms));
		}
		elseif(count($rooms) == 0){
			echo "номер не найден";
			$result = addRoom($hotel, $sku);
		}
		elseif(count($rooms) == 1){			
		



//\Bitrix\Catalog\ProductTable::TYPE_OFFER
/*		
		$arFields = [
		'TYPE' => 4,
		"ID" => $rooms[0]['ID'],
		'IBLOCK_ID' => 8
		];
		$res = CCatalogProduct::Add($arFields);
		print_r($res);
		//print_r(\Bitrix\Catalog\ProductTable::TYPE_OFFER);
		die();
		/*
		//$ar_fields = $res->GetNext();
		$roomsCatalog = \CIBlockElement::getProperty(8, 393, array("sort", "asc"), array('TYPE' => 'TYPE_SKU'));			
		while($arProperty = $roomsCatalog->GetNext()){
			print_r($arProperty);	
		}
		
		die();
		*/
		
		
		
		
		
		
		
		
			echo "номер найден";
			print_r($rooms);
			$result = [
				'id' => $rooms[0]['ID'],
				'categoryid' => $rooms[0]['IBLOCK_ID'],
				'name' => $rooms[0]['NAME'],
				'catalogid' => $rooms[0]['IBLOCK_SECTION_ID'],
				'code' => $rooms[0]['CODE'],
			];
		}
		return $result;
	
}
function addRoom($hotel, $sku){

	$PROP['TYPE'] = "TYPE_SKU";
	
	//die();
	$arLoadProductArray = Array(
		"IBLOCK_SECTION_ID" => $hotel['catalogid'], //Тут надо поставить условие куда кидаем что   GetSectionID($Array['parent']) или SUMMER,          // элемент лежит в корне раздела
		"IBLOCK_ID"      => IBLOCKROOM,
		"PROPERTY_VALUES"=> $PROP,
		"NAME"           => $sku['roomname'],
		"ACTIVE"         => "N",            // активен
	);
	
	$el = new CIBlockElement;
	if($PRODUCT_ID = $el->Add($arLoadProductArray)){
		$result = [
				'id' => $PRODUCT_ID,
				'categoryid' => $hotel['categoryid'],
				'name' => $sku['roomname'],
				'catalogid' => $hotel['catalogid'],
				//'code' => $rooms[0]['CODE'],			
		];
		//\Bitrix\Catalog\ProductTable::TYPE_SKU
		echo "New ID: ".$PRODUCT_ID;
		/*
		$arFields = array(
						  "ID" => $PRODUCT_ID, 
						  "TYPE_SKU" => 
						  //"VAT_ID" => 1, //выставляем тип ндс (задается в админке)  
						  //"VAT_INCLUDED" => "Y" //НДС входит в стоимость
						  );
		if(CCatalogProduct::Add($arFields))
			echo "Добавили параметры товара к элементу каталога ".$PRODUCT_ID.'<br>';
		else
			echo 'Ошибка добавления параметров<br>';		
		*/
		return $result;
	}
	else{
		echo "Error: ".$el->LAST_ERROR;
		return false;
	}
	//echo "add element:<br>";
	
}





function cnv($data){
	$res = mb_convert_encoding($data, 'UTF-8', "windows-1251");
	return $res;
}

function GetFoodAndDatе($arr){
	$tmp = array();
	$result = array(
		'food' => null,
		'period' => array()
		);
	$result['food'] = $arr[0];
	$i = 0;
	foreach ($arr as &$val){
		if($i != 0){
			$tmp[] =getPeriod($val);
		}
		$i++;
	}
	//$result['period'] = $tmp;
	$result['period'] = $tmp;
	return $result;
}

function getPeriod($dateStr){
	$dates = explode("-", $dateStr);
	if(count($dates) == 1){
		$tmp = new DateTime($dates[0]);
		$result['startdate'] = $tmp->format("Y-m-d 00:00:00");
		$result['enddate'] = $tmp->format("Y-m-d 23:59:59");
	}
	else{
		$startdate = new DateTime($dates[0]);
		$enddate = new DateTime($dates[1]);
		$result['startdate'] = $startdate->format("Y-m-d 00:00:00");
		$result['enddate'] = $enddate->format("Y-m-d 23:59:59");
	}
	return $result;
}
/*

die();

define("REST", 27);
define("BAR", 29);
define("HOTEL", 20);
define("TODO", 39);
define("WINTER", 36);
define("SUMMER", 37);
define("SERVICE", 38);

$IMPORT_OBJECT = 'hotels'; // hotels, food, todo, winter, summer, service

$f_json = 'https://krasnayapolyanaresort.ru/appdata.json?element='.$IMPORT_OBJECT;
switch($IMPORT_OBJECT){
	case 'food':
		define("IBLOCK", 9); //Категория
	break;
	case 'hotels':
		define("IBLOCK", 7); //Категория
	break;	
	case 'todo':
		define("IBLOCK", 11); //Категория
	break;	
	case 'winter':
		define("IBLOCK", 11); //Категория
	break;	
	case 'summer':
		define("IBLOCK", 11); //Категория
	break;	
	case 'service':
		define("IBLOCK", 11); //Категория
	break;	
	
}



$json = file_get_contents($f_json);
$arJSON = json_decode($json, true);
$i = 0;
$y=0;
foreach($arJSON as &$arItem){
	if(!CheckItem($arItem)){
		AddItem($arItem);
		$i++;
	}
	else{
		//print_r($arItem);
		//echo "Обьект не загружен";
		echo "<br>";
	}
	$y++;
	//if($i > 0) break;
}
echo "Импортировано $i обьектов из $y";
*/
?>