<?
//$params = $_REQUEST['params'];
/*
Передаваемые параметры
{
  offset:,
  categoryId:,
  subcategoryId:, //Необязательно
}
*/


function GetPartnersByCategory(){
	$params = $_REQUEST['params'];
	$params = array(
	  "offset" => 0,
	  "categoryId" => 7,
	  //"subcategoryId" => 21, 
	);
	$Partners = GetPartners($params['categoryId'], $params['subcategoryId']);
	//echo "<pre>";
	return $Partners;
}


function GetPartners($categoryId, $subcategoryId){
	
	$arResult = array();

	$arSelect = Array("ID", "NAME", "IBLOCK_NAME", "IBLOCK_SECTION_ID", "IBLOCK_ID", "IBLOCK_CODE", "CODE");

	$arFilter = Array("IBLOCK_ID"=>$categoryId, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "IBLOCK_SECTION_ID" => $subcategoryId);

	$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
	while($ob = $res->GetNextElement())
	{
		$arFields = $ob->GetFields();
		
		$prop=CIBlockElement::GetByID($arFields['ID'])->GetNextElement()->GetProperties();
		//print "<pre>"; print_r($prop); print "</pre>";

		//$iterator = CIBlockElement::GetPropertyValues(7, array('ID' => $arFields['ID'], 'ACTIVE' => 'Y'), true, array());
		//$ar = array();
		//while ($row = $iterator->Fetch())
		//{
			//array_push($ar, $row);
		//}
//		$arProp = [];
		$arPhoto = array('small' => array(),'mid' => array());
		foreach ($prop as &$prop_value) {
			if ($prop_value['CODE'] == 'photos'){
				
				//$img_path = CFile::GetPath($value['VALUE']);
				/*
				$img_path = CFile::ResizeImageGet(
													$value['VALUE'][0],
													//array('width'=>'360', 'height'=>'150'), //720
													array('width'=>'360','height'=>'150'), //720
													BX_RESIZE_IMAGE_PROPORTIONAL,
													true
												);
				*/			

				foreach ($prop_value['VALUE'] as &$photo_value) {
					//$img_path = CFile::GetPath($photo_value);
					$img_path_small = CFile::ResizeImageGet($photo_value, array('width'=>360, 'height'=>360), BX_RESIZE_IMAGE_PROPORTIONAL, true);
					$img_path_mid = CFile::ResizeImageGet($photo_value, array('width'=>720, 'height'=>720), BX_RESIZE_IMAGE_PROPORTIONAL, true);

					array_push($arPhoto['small'], $img_path_small['src']);
					array_push($arPhoto['mid'], $img_path_mid['src']);
					//print_r($img_path_small);
				}
			}
			
			$arProp[$prop_value['CODE']] 
							= $prop_value['VALUE'];
								/*array(
									'ID' => $prop_value['ID'], 
									'CODE' => $prop_value['CODE'], 
									'NAME' => $prop_value['NAME'], 
									'VALUE' => $prop_value['VALUE']
									);				*/
		}
		
		array_push($arResult, 
							array(
									"id" => $arFields['ID'], 
									"title" => $arFields['NAME'], 
									"partnerId" => $arFields['IBLOCK_SECTION_ID'], 
									"subtitle" => $arFields['IBLOCK_NAME'],
									//"prop" => $prop,
									"prop" => $arProp,
									"photos" => $arPhoto,
									"price" => rand(1000, 100000),
								)
					);
	}

	return $arResult;

}




?>
