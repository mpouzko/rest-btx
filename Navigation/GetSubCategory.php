<?php

function GetSubCategory($params){
	$arResult = array();
	$res = CIBlock::GetList(
		Array(), 
		Array(
			'TYPE'=>'catalog', 
			'SITE_ID'=>SITE_ID, 
			'ACTIVE'=>'Y', 
			"CNT_ACTIVE"=>"Y", 
			//"!CODE"=>'my_products'
		), true
	);
	//print_r($ar_res);

	while($ar_res = $res->Fetch())
	{
		$arCatResult = array();
		if($ar_res['ID'] != 8){
			$img_path_small = CFile::ResizeImageGet($ar_res['PICTURE'], array('width'=>760, 'height'=>760), BX_RESIZE_IMAGE_PROPORTIONAL, true);
			$img_path_mid = CFile::ResizeImageGet($ar_res['PICTURE'], array('width'=>1160, 'height'=>1160), BX_RESIZE_IMAGE_PROPORTIONAL, true);

			$arPhoto['small'] = SERVER_NAME_HTTP.$img_path_small['src'];
			$arPhoto['mid'] = SERVER_NAME_HTTP.$img_path_mid['src'];

			if(!$img_path_small['src'])
				$arPhoto['small'] = DEFAULT_PRICTURE;
			if(!$img_path_mid['src'])
				$arPhoto['mid'] = DEFAULT_PRICTURE;


				$arCatResult = array (
					"id" => $ar_res['ID'],
					"name" => $ar_res['NAME'],
					"code" => $ar_res['CODE'],
					"icon" => $ar_res['DESCRIPTION'],
					"picture" => $arPhoto,
					"subcat" => array()
				);
				
				$arFilter = Array('IBLOCK_ID'=>$ar_res['ID'], 'ACTIVE'=>'Y');
				
				$resSub = CIBlockSection::GetList(array(), $arFilter);
				while($ar_subres = $resSub->Fetch())
				{
					$ar_subcat = array();
					
					$ar_subcat = array(
					//"ID" => 123,
						"id" => $ar_subres['ID'],
						"code" => $ar_subres['CODE'],
						"name" => $ar_subres['NAME'],
					);
					
					//echo "<pre>";//$ar_res['NAME'].': '.$ar_res['ELEMENT_CNT'];
					//print_r($ar_res);
					array_push($arCatResult['subcat'], $ar_subcat);
				}
				array_push($arResult, $arCatResult);
				
			//}
		}
	}

	return $arResult;
}


?>