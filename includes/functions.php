<?
//--------------Функции //Статика - статичный набор свойств обьекта, которые требуется обрабатывать нестандартным способом
function process_fields($Field){
	$result = null;					
	$arPhoto = [];
	switch($Field['CODE']){

		case 'services_desc':
			$result = $Field['VALUE']['TEXT'];
		break;
		case 'note':
			$result = $Field['VALUE']['TEXT'];
		break;
		case 'terms':
			$result = $Field['VALUE']['TEXT'];
		break;
		case 'labels':
			$dbProperty = \CIBlockElement::getProperty(12, $Field['VALUE'], array("sort", "asc"), 'ID');//Статика
			while ($arProperty = $dbProperty->GetNext()) {
			   $prop_value[$arProperty['CODE']] = $arProperty['VALUE'];
			}		
			$result = $prop_value;
			//$result = $Field['VALUE_ENUM'];
		break;
		case 'services':
			$dbProperty = \CIBlockElement::getProperty(13, $Field['VALUE'], array("sort", "asc"), 'ID'); //Статика
			while ($arProperty = $dbProperty->GetNext()) {
			   $prop_value[$arProperty['CODE']] = $arProperty['VALUE'];
			}		
			$result = $prop_value;
			//$result = $Field['VALUE_ENUM'];
		break;		
		case 'stars':
			$result = $Field['VALUE_ENUM'];
		break;		
		
		case 'photos':
			$result = GetPhotoById($Field['VALUE']);
		break; 
		case 'products':
			//GetProductCatalogByPartnerCode
			//$result = $Field['VALUE']['TEXT'];
			//$result = $this->ID ;
		break;

		default:
			$result = $Field['VALUE'];
		break;
	}
	return $result;
}

function GetProducts($productId, $CategoryId, $SectionRoomId, $PartnerCategory, $PartnerSubCategory, $params){
	if($CategoryId == BOOKING)
		//$Iblock = GetProductBlockId($CategoryId);
		$Iblock['id'] = BOOKING_ROOMS;
	else{
		//все кроме бронирования
		$result = GetSKU($productId);
		return $result;
	}


	$filter = array('IBLOCK_ID' => $Iblock['id'], 'IBLOCK_SECTION_ID' => $SectionRoomId, "ACTIVE"=>"Y");


		//Получаем все товары находящиеся в секции указанной у партнера в свойствах
		$dbItem = \Bitrix\Iblock\ElementTable::getList(array(
			'select' => array('ID', 'IBLOCK_ID', 'NAME', 'IBLOCK_SECTION_ID', 'CODE', 'PREVIEW_TEXT'),
			//'select' => array(),
			'filter' => $filter,
			//'limit' => PAGE_LIMIT,
			//'offset' => $offset,
			'order' => array('TIMESTAMP_X' => 'ASC')
		));

		while ($arItem = $dbItem->fetch()) {
			$arItemRes=[];
			$arItemRes['id'] = $arItem['ID'];
			$arItemRes['name'] = $arItem['NAME'];
			$arItemRes['category_id'] = $PartnerCategory;
			$arItemRes['subcategory_id'] = $PartnerSubCategory;
			$arItemRes['code'] = $arItem['CODE'];
			$dbProperty = \CIBlockElement::getProperty($arItem['IBLOCK_ID'], $arItem['ID'], array("sort", "asc"), []);//GetPropArray($Iblock['CODE'])); //GetPropArray($Iblock['CODE'])
			while ($arProperty = $dbProperty->GetNext()) {
				if ($arProperty['VALUE']) {
					if($arProperty['CODE'] == 'photos' || $arProperty['CODE'] == 'labels')
						$arItemRes['params'][$arProperty['CODE']][] = process_fields($arProperty);
					else
						$arItemRes['params'][$arProperty['CODE']] = process_fields($arProperty);
				}
			}
			if(!$arItemRes['services_desc'])
				$arItemRes['services_desc'] = $arItem['PREVIEW_TEXT'];
			$dateIn = new datetime($params['dateIn']);
			$dateOut = new datetime($params['dateOut']);
			$summ = 0;
			$price = [];
			for ($i = $dateIn; $i <= $dateOut; date_add($dateIn, date_interval_create_from_date_string('1 days'))) {
				$price[] = [getPrice($arItem['ID'], $params, $dateIn->format('Y-m-d')), $dateIn->format('Y-m-d'), $params];
				$summ += (int)getPrice($arItem['ID'], $params, $dateIn->format('Y-m-d'));
			}
			unset($dateIn);
			unset($dateOut);
			$arItemRes['price'] = $summ;
			$arItemRes['priceAll'] = $price;
			$arItems[] = $arItemRes;
		}
		return $arItems;
		
}

//----------Жесткий кодинг не используется

function GetProductBlockId($CategoryId){
	$result = null;
	switch($CategoryId){
		case 7: // Проживание
			$result['id'] = 8; //Статика
			$result['CODE'] = 'ROOMS';
			
		break;
		case 9: // Питание
		break;
		case 11: // Развлечение
		break;
	}
	return $result;
}

//Статика, используется только в конструкторе класса для создания обязательных полей, но так как есть обработка - можно тупо отправлять пустой массив как в default
function GetPropArray($CODE){
	$arRes = array();
	switch ($CODE){
		case 'ACC':
			array_push($arRes,
					['CODE' => 'stars'],
					['CODE' => 'rating'],
					['CODE' => 'labels'],
					['CODE' => 'address'],
					['CODE' => 'services'],
					['CODE' => 'photos'],
					['CODE' => 'position'],
					['CODE' => 'services_desc'],
					['CODE' => 'terms'],
					['CODE' => 'note'],
					['CODE' => 'hint'],
					['CODE' => 'products'],
					['CODE' => 'base_price']

				);
		break;
		case 'FOOD':
			array_push($arRes,
					['CODE' => 'rating'],
					['CODE' => 'labels'],
					['CODE' => 'address'],
					['CODE' => 'services'],
					['CODE' => 'avgcheck'],
					['CODE' => 'contacts'],
					['CODE' => 'photos'],
					['CODE' => 'position'],
					['CODE' => 'services_desc'],
					['CODE' => 'note']
					//'CODE' => 'hint',
					);		
		break;
		case 'ROOMS':
			array_push($arRes,
					['CODE' => 'capacity'],
					['CODE' => 'square'],
					['CODE' => 'description'],
					['CODE' => 'photos'],
					['CODE' => 'base_price']
					//'CODE' => 'hint',
					);		
		break;
		case 'SKIPASS':
			array_push($arRes,
					//['CODE' => 'capacity'],
					//['CODE' => 'square'],
					['CODE' => 'description'],
					['CODE' => 'photos'],
					['CODE' => 'base_price']
					//'CODE' => 'hint',
					);		
		break;	
		case 'CHILDREN':
			array_push($arRes,
					//['CODE' => 'capacity'],
					//['CODE' => 'square'],
					['CODE' => 'description'],
					['CODE' => 'photos'],
					['CODE' => 'base_price']
					//'CODE' => 'hint',
					);		
		break;	
		case 'RIDES':
			array_push($arRes,
					//['CODE' => 'capacity'],
					//['CODE' => 'square'],
					['CODE' => 'description'],
					['CODE' => 'photos'],
					['CODE' => 'base_price']
					//'CODE' => 'hint',
					);		
		break;	
		case 'TOURS':
			array_push($arRes,
					//['CODE' => 'capacity'],
					//['CODE' => 'square'],
					['CODE' => 'description'],
					['CODE' => 'photos'],
					['CODE' => 'base_price']
					//'CODE' => 'hint',
					);		
		break;								
		default:
			$arRes = [
			]	;	
		break;
	}
	
	return $arRes;	
	
}
function GetSKU($id){
	$skuFilter = array();
	$fields = [
		'NAME',
		'CODE',
		'ID',
		'PREVIEW_PICTURE',
		'PREVIEW_TEXT',
		'CATALOG_PRICE_1',
		//'PREVIEW_TEXT'
	];	
	$res1 = CCatalogSKU::getOffersList(
		[$id], // массив ID товаров
		$iblockID = 0, // указываете ID инфоблока только в том случае, когда ВЕСЬ массив товаров из одного инфоблока и он известен
		$skuFilter, // дополнительный фильтр предложений. по умолчанию пуст.
		$fields,  // массив полей предложений. даже если пуст - вернет ID и IBLOCK_ID
		$propertyFilter = array() /* свойства предложений. имеет 2 ключа:
							ID - массив ID свойств предложений
									либо
							CODE - массив символьных кодов свойств предложений
									если указаны оба ключа, приоритет имеет ID*/
	);
	$res = array_shift($res1);
	foreach($res as $item){
		$result[] = [
			'id' => $item['ID'],
			'name' => $item['NAME'],
			'code' => $item['CODE'],
			//'category_id' => null,
			//'subcategory_id' => null,
			'params' => ['photos' => [GetPhotoById($item['PREVIEW_PICTURE'])]],
			'catalog' => $item['IBLOCK_ID'],
			'price' => (int)$item['CATALOG_PRICE_1'], //CATALOG_PRICE_ID_1
			'services_desc' => $item['PREVIEW_TEXT'], //CATALOG_PRICE_ID_1
			//'name' => $item['NAME'],
		];
	}
	return $result;
}


/*
function GetPriceOld($data, $id, $name){
/*
		  "offset" => 0,
		  "categoryId" => 7,
		  "dateIn" => "2019-12-01",
		  "dateOut" => "2019-12-10",
		  "adults" => 2,
		  "children" => 3,
		  "time" => "10:10:10",
*/
/*	if($data['children'] == 0)
		$capacity = $data['adults'];
	else {
		//$capacity = [
			//$data['adults']."+".$data['children']."(7-12)",
			//$data['adults']."+".$data['children']."(7-15)",
		//];
		$capacity = $data['adults']."+".$data['children']."(7-12)";
	}
	$capacity = "2";
	
	
	//$price = rand(10000, 100000);
	//$newprice = $price - 5000;	
	$result = GetPriceSQL($data, $capacity, $name);
	//return "0"; 
	return $result; //$capacity;

}

function GetBestPrices($data){
	/*
			  "offset" => 0,
		  "categoryId" => 7,
		  "dateIn" => "2019-12-01",
		  "dateOut" => "2019-12-10",
		  "adults" => 2,
		  "children" => 3,
		  "time" => "10:10:10",
*/
/*	if($data['children'] == 0)
		$capacity = $data['adults'];
	else {
		//$capacity = [
			//$data['adults']."+".$data['children']."(7-12)",
			//$data['adults']."+".$data['children']."(7-15)",
		//];
		$capacity = $data['adults']."+".$data['children']."(7-12)";
	}
	$capacity = "2";
	
	
	//$price = rand(10000, 100000);
	//$newprice = $price - 5000;	
	$result = GetBestPricesSQL($data, $capacity);
	//return "0"; 
	return $result; //$capacity;
}

function GetBestPricesSQL ($data, $capacity){
	$query = "select 
				min(price) as price
				,hotelname
                ,M.name_price
                ,M.name_bitrix
			from hotel_prices.price 
			left join hotel_prices.mapping as M on M.name_price = hotelname
			where 
				guests = '".$capacity."' 
				and
				( 
					(startdate >= '".$data['dateIn']."' and startdate <= '".$data['dateOut']."')
					or
					(enddate >= '".$data['dateIn']."' and enddate <= '".$data['dateOut']."')
				)
				group by hotelname, M.name_price, M.name_bitrix";
	
	
	$mysqli = new mysqli("localhost", "root", "QZWxxv3GRN", "hotel_prices");
	/* проверка соединения */
/*	if ($mysqli->connect_errno) {
		printf("Не удалось подключиться: %s\n", $mysqli->connect_error);
		$tmp = $mysqli->connect_error;
		//return "$query";
		return;
		exit();
	}
	if ($result = $mysqli->query($query)) {
		while ($row = $result->fetch_assoc()){
			$res[] = $row;
		}
	}
	else
		return;
	//return "$query";
	$mysqli->close();
	//return "0";
	return $res;
}


function GetPriceSQL($data, $capacity, $name){
	$query = "select 
				min(price) as price
			from hotel_prices.price 
			left join hotel_prices.mapping as M on M.name_bitrix = '".$name."'
			where 
				guests = '".$capacity."' 
				and
				( 
					(startdate >= '".$data['dateIn']."' and startdate <= '".$data['dateOut']."')
					or
					(enddate >= '".$data['dateIn']."' and enddate <= '".$data['dateOut']."')
				)
				and CASE WHEN M.name_price is null then hotelname = '".$name."' else hotelname = M.name_price end
				group by hotelname";
	
	
	$mysqli = new mysqli("localhost", "root", "QZWxxv3GRN", "hotel_prices");
	/* проверка соединения */
/*	if ($mysqli->connect_errno) {
		printf("Не удалось подключиться: %s\n", $mysqli->connect_error);
		$tmp = $mysqli->connect_error;
		//return "$query";
		return;
		exit();
	}
	if ($result = $mysqli->query($query)) {
		if($result->num_rows == 0)
			return;
		//return "$query";
		$row = $result->fetch_assoc();
	}
	else
		return;
	//return "$query";
	$mysqli->close();
	//return "0";
	return $row['price'];
	//return "$query"; //$row['price'];
}
*/
function ParceGuests($value){
	//echo "<br>Гости<br>";
	//print_r($value);
	//$value = "3+2(0-6)(7-15)";
	$result = ['childs' => 0, 'age' => null];
	$arr = explode(" ", $value);	
	if($arr){
		$adults = explode("+", $value);
		$result['adults'] = substr($adults[0], 0, 1);
		$result['childs_total'] = substr($adults[1], 0, 1);
		if(count($adults) > 1){
			$childs = explode("(", $adults[1]);
			if(count($childs) > 2){
				//echo "детей более 1 вида";
				$i = 0;
				foreach($childs as $chage){
					if($i > 0){
						$result['age'][] = substr($chage, 0, 1);
						if(substr($chage, 0, 1) > 0){
							$result['childs']++;
						}
					}
					$i++;
				}
			}
			else {
				//echo "детей 1 вид";
				$age = substr($childs[1], 0, 1);
				$result['age'][] = $age;
				if($age > 0){
					$result['childs'] = substr($adults[1], 0, 1);
				}
			}
		}
	}
	else {
		$arr = explode("(", $value);	
		$result = ['childs' => 0, 'age' => null, 'childs_total' => 0, 'adults' => substr($value, 0, 1)];
	}
	return $result;
}

/******* Работа с фото START */
function GetPhotoById($id){
	$img_path_small = CFile::ResizeImageGet($id, array('width'=>SMALL_IMG_W, 'height'=>SMALL_IMG_H), BX_RESIZE_IMAGE_EXACT , true);
	$img_path_mid = CFile::ResizeImageGet($id, array('width'=>MID_IMG_W, 'height'=>MID_IMG_H), BX_RESIZE_IMAGE_EXACT, true); //BX_RESIZE_IMAGE_PROPORTIONAL
	$arPhoto['small'] = SERVER_NAME_HTTP.$img_path_small['src'];
	$arPhoto['mid'] = SERVER_NAME_HTTP.$img_path_mid['src'];
	return $arPhoto;	
}
/******* Работа с фото END*/


/******* Различные лукапы - вспомогательные функции START */
function LookupIBLOCK_OFFER($idblock){

	if($idblock && $idblock != ""){
		$mxResult = CCatalogSKU::GetInfoByProductIBlock($idblock);
		if (is_array($mxResult))
		{
			return [(int)$idblock, (int)$mxResult['IBLOCK_ID']];
		}
		else
		{
			if($idblock == BOOKING)
			/*******можно дописать логику чтобы искал по номерам если это бронирование*/
				return [(int)$idblock];
			else
				return [(int)$idblock];			
		}
	}
	else{
		$res = CIBlock::GetList(
			Array(), 
			Array(
				'TYPE'=>[PARTNER,PRODUCT],//PARTNER,//PRODUCT, 
				'SITE_ID'=>LANG, 
				'ACTIVE'=>'Y', 
			), true
		);
		while($ar_res = $res->Fetch())
		{
			$result[] = (int)$ar_res['ID'];
		}		
		return $result;		
	}
}

function GetRoomsSectionID($HotelId){
	$dbProperty = \CIBlockElement::getProperty(BOOKING, $HotelId, array("sort", "asc"), ['CODE' => 'products']);
	while ($arProperty = $dbProperty->GetNext()) {
		return $arProperty['VALUE'];
	}
}

function LookupIBLOCK($ID = null, $CODE = null){
	if($ID)
		$filter = ["ID" => $ID];
	elseif($CODE)
		$filter = ["CODE" => $CODE];
	else
		return false;
	$iblocks = GetIBlockList("catalog", $filter);
	while($arIBlock = $iblocks->GetNext()) //цикл по всем блокам
	{
	   // здесь мы НЕ используем функцию htmlspecialchars($arIBlock["NAME"]), 
	   // т.к. метод GetNext() делает это за нас
	   $result = ['ID' => $arIBlock["ID"], 'CODE' => $arIBlock["CODE"]]; 
	}	
	return $result;
}

function LookupItem($ID = null, $CODE = null, $IBLOCK_ID){
	if($ID)
		$filter = ["ID" => $ID];
	elseif($CODE)
		$filter = ["CODE" => $CODE, "IBLOCK_ID" => $IBLOCK_ID];
	else
		return false;
	$items = CIBlockElement::GetList([], $filter);
	while($Item = $items->GetNext()) //цикл по всем блокам
	{
	   $result = ['ID' => $Item["ID"], 'CODE' => $Item["CODE"]]; 
	}	
	return $result;
}

//Статика
function LookupSkuPerson($code, $value){
	switch ($code){
		case "adults":
			switch($value){
				case 1:
					return 45;
				break;	
				case 2:
					return 46;	
				break;	
				case 3:
					return 47;
				break;	
				case 4:
					return 48;
				break;					
				case 5:
					return 58;
				break;					
				case 6:
					return 59;
				break;					
				case 7:
					return 60;
				break;					
				case 8:
					return 61;
				break;					
			}
		break;
		case "childs":
			switch($value){
				case 0:
					return 63;
				break;	
				case 1:
					return 49;
				break;	
				case 2:
					return 50;	
				break;	
				case 3:
					return 51;
				break;	
				case 4:
					return 62;
				break;	
				default:
					return 0;
				break;
			}
		break;
		case "food":
			switch($value){
				case "BB":
					return "52";
				break;	
				case "HB":
					return "53";
				break;	
				case "RO":
					return "54";
				break;	
			}
		break;
	}
}
/******* Различные лукапы - вспомогательные функции END */

/*******************Функции работы с ценами START*/

/*******************Работает по аналогии с BestProducts, возможно обьединить их*/
function GetHotelBestPrice($HotelId, $params){
	$skuFilter = [
		'<=PROPERTY_startdate' => $params['dateIn'],
		'>=PROPERTY_enddate' => $params['dateIn'],
		'PROPERTY_ADULTS_VALUE' => $params['adults'],
		'PROPERTY_CHILDREN_VALUE' => $params['children'],
		'NAME' => $HotelId,
		"IBLOCK_ID" => BOOKING_OFFERS, 
		"ACTIVE"=>"Y", 
	];
	$db_price_min = CIBlockElement::GetList(
		array("CATALOG_PRICE_1" => "ASC"), 
		$skuFilter, 
		false, 
		array("nPageSize"=>1),
		array()
	);
	while($ob_price_min = $db_price_min->GetNextElement())
	{
		$arPrices = $ob_price_min->GetFields();
		return $arPrices['CATALOG_PRICE_1'];
	}
}

function GetPrice($id, $params, $dateIn = null){
	//return "9991";
	//Торговые ли предложения
	//if($params['categoryId'] == BOOKING){
		//$arrRooms = GetRoomsID($id);
	//	return GetHotepBestPrice($id, $params);
	//}
	//if($id == 10557)

	//$products = GetProducts(null, $params['category_id'], $arProperty, $arItem['IBLOCK_ID'], $arItem['IBLOCK_SECTION_ID'], $params);

	/**имеются ли у элемента торговые предложения. */
	if(CCatalogSKU::IsExistOffers($id)){
		//$array = [$params, $id];
		if(!$dateIn){
			$skuFilter = array(
				'<=PROPERTY_startdate' => $params['dateIn'],
				'>=PROPERTY_enddate' => $params['dateIn'],
				'PROPERTY_ADULTS_VALUE' => $params['adults'],
				'PROPERTY_CHILDREN_VALUE' => $params['children'],
			);
		}
		else{
			$skuFilter = array(
				'<=PROPERTY_startdate' => $dateIn,
				'>=PROPERTY_enddate' => $dateIn,
				'PROPERTY_ADULTS_VALUE' => $params['adults'],
				'PROPERTY_CHILDREN_VALUE' => $params['children'],
			);
			
		}
		$fields = [
			'PROPERTY_startdate',
			'PROPERTY_enddate',
			'PROPERTY_guests',
			'PROPERTY_adults',
			'PROPERTY_children',
			'PROPERTY_food',
			'CATALOG_PRICE_1'
		];
		//LogData("GetPrice", $skuFilter, DBG);
		$res = CCatalogSKU::getOffersList(
			[$id], // массив ID товаров
			$iblockID = 0, // указываете ID инфоблока только в том случае, когда ВЕСЬ массив товаров из одного инфоблока и он известен
			$skuFilter, // дополнительный фильтр предложений. по умолчанию пуст.
			$fields,  // массив полей предложений. даже если пуст - вернет ID и IBLOCK_ID
			$propertyFilter = array() /* свойства предложений. имеет 2 ключа:
								ID - массив ID свойств предложений
										либо
								CODE - массив символьных кодов свойств предложений
										если указаны оба ключа, приоритет имеет ID*/
		);
	
		return array_shift(array_shift($res))['CATALOG_PRICE_1']; //Умножить на  к-во дней
	}
	//Не имеет торговых предложений или сам является предложением
	else{
		$ar_res = CPrice::GetBasePrice($id);
		return $ar_res['PRICE'];
	}
}
/*******************Функции работы с ценами END*/



?>