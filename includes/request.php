<?
//echo "headers";
function GetParams(){
	if($_REQUEST['params']){
		$params = $_REQUEST['params'];
	}
	else{
		$postData = file_get_contents('php://input');
		$POST = json_decode($postData, true);		
		$params = $POST['params'];
		if($POST['params'][DBG] == 1)
			$_REQUEST[DBG] = 1;
	}
	if(array_key_exists("sort", $params)){
		if($params['sort'] != "ASC" || $params['sort'] != "DESC")
			$params['sort'] = "ASC";
	}
	else {
		$params['sort'] = "ASC";
	}

	/** Это можно переделать после того, как будут передаваться параметры на всех страницах */
	if(array_key_exists("dateIn", $params)){
		if(validateDate($params['dateIn']) == false)
			$params['dateIn'] = date("Y-m-d");
	}
	else {
		$params['dateIn'] = date("Y-m-d", strtotime(date("Y-m-d").' +0 day'));
	}
	if(array_key_exists("dateOut", $params)){
		if(validateDate($params['dateOut']) == false)
			$params['dateOut'] = date("Y-m-d");
	}
	else {
		$params['dateOut'] = date("Y-m-d", strtotime(date("Y-m-d").' +1 day'));
	}		
	if(array_key_exists("adults", $params)){
		if(validateDate($params['adults']) == false)
			$params['adults'] = 2;
	}
	else {
		$params['adults'] = 2;
	}	
	if(array_key_exists("children", $params)){
		if(validateDate($params['children']) == false)
			$params['children'] = 0;
	}
	else {
		$params['children'] = 0;
	}				
	return $params;

}
//function validateDate($date, $format = 'Y-m-d H:i:s')
function validateDate($date, $format = 'Y-m-d')
{
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
}	
?>