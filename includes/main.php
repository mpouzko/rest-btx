<?
//header('Location: http://www.example.com/');
//echo "12313";
include ('../bitrix/modules/main/include/prolog_before.php');
CModule::IncludeModule("iblock");
CModule::IncludeModule("catalog");
CModule::IncludeModule("sale");
CModule::IncludeModule("im");

include (dirname(__FILE__).'../../includes/config.php');
include (dirname(__FILE__).'../../includes/entities.php');

if($_SERVER['HTTP_ORIGIN'])
    $http_origin = $_SERVER['HTTP_ORIGIN'];
else
    //$http_origin = "http://localhost:3000";
	$http_origin = "*";
//print_r($_SERVER);
//header("Access-Control-Allow-Origin: $http_origin");
header('Access-Control-Allow-Origin: '.$http_origin);
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: POST, GET');
header('Access-Control-Allow-Headers: Content-Type, Set-Cookie, *');
//header('Content-Type: application/json');
header('Content-Type: text/plain');

//setcookie("TestCookie", $value);

?>