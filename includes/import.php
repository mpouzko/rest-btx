<?

function CheckItem($Array){
	if($Array['context'] != 'ru')
		return true;
	//print_r($Array['id']);
	$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM", "modexid");
	$arFilter = Array("IBLOCK_ID" => IBLOCK, "PROPERTY_modexid"=>$Array['id'], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
	//print_r($res->result->num_rows);
	if($res->result->num_rows == 0)
		return false;
	while($ob = $res->GetNextElement())
	{
	 $arFields = $ob->GetFields();
	 //print_r($arFields);
	}	
	return true;
}

function AddItem($Array){
	//print_r($Array);
	$resPhotos = getPhotosAndDesc($Array['id']);
	$PROP['address'] = array("VALUE" => $Array['address']);
	$PROP['modexid'] = array("VALUE" => $Array['id']);
	$PROP['avgcheck'] = array("VALUE" => $Array['minprice']);
	$PROP['contacts'] = array("VALUE" => "тел.: ".$Array['phone']." e-mail: ".$Array['email']);
	$PROP['position'] = array("VALUE" => NormolizePosition($Array['position'])); 
	$PROP['yaposition'] = array("VALUE" => NormolizePosition($Array['position'])); 
	
	$PROP['stars'] = array("VALUE" => GetStars($Array['stars'])); 
	$PROP['services_desc'] = array("VALUE" => html_entity_decode(html_entity_decode($resPhotos['mainContent']." Время работы: ".$resPhotos['workhours']))); 
	$PROP['terms'] = array("VALUE" => html_entity_decode(html_entity_decode($resPhotos['servicesContent']))); //Содержится только в отелях
	$PROP['note'] = array("VALUE" => html_entity_decode(html_entity_decode($resPhotos['roomsContent']))); //Содержится только в отелях
	

	foreach($resPhotos['photos'] as &$resPhoto){
		$Photos[] = CFile::MakeFileArray($resPhoto);
	}
	$PROP['photos'] = $Photos;
	
	
	$arLoadProductArray = Array(
		"IBLOCK_SECTION_ID" => GetSectionID($Array['parent']), //Тут надо поставить условие куда кидаем что   GetSectionID($Array['parent']) или SUMMER,          // элемент лежит в корне раздела
		"IBLOCK_ID"      => IBLOCK,
		"PROPERTY_VALUES"=> $PROP,
		"NAME"           => html_entity_decode($Array['name']),
		"ACTIVE"         => "Y",            // активен
	);
	
	$el = new CIBlockElement;
	if($PRODUCT_ID = $el->Add($arLoadProductArray))
		echo "New ID: ".$PRODUCT_ID;
	else
		echo "Error: ".$el->LAST_ERROR;
	echo "add element:<br>";

}

function GetSectionID($NAME){
	switch($NAME){
		case "Рестораны":
			$ID = REST;
		break;
		case "Кафе и бары":
			$ID = BAR;
		break;
		case "Круглый год":
			$ID = SUMMER;
		break;		
		case "Зима":
			$ID = WINTER;
		break;		
		case "What to do":
			$ID = TODO;
		break;		
		case "Красота и здоровье":
			$ID = SERVICE;
		break;	
		case "Bowling":
			$ID = SERVICE;
		break;	
		case "ТРЦ Горки Молл":
			$ID = SERVICE;
		break;	
		case "Отдых с детьми":
			$ID = SERVICE;
		break;	
		case "Забронировать отель":
			$ID = HOTEL;
		break;		
		default:
			$ID = null;
		break;
	}
	return $ID;
}


function getPhotosAndDesc($ID){
	$returnArray = [];
	$url_photo = "https://krasnayapolyanaresort.ru/appdata.json?element=item-extended&id=".$ID;
	$json = file_get_contents($url_photo);
	$array = json_decode($json, true);
	//print_r($array);
	$returnArray['roomsContent'] = $array['roomsContent'];
	if($array['mainContent'] == "")
		$returnArray['mainContent'] = $array['introtext'];
	else
		$returnArray['mainContent'] = $array['mainContent'];
	$returnArray['servicesContent'] = $array['servicesContent'];
	//echo "--------";
	$arPhotos = [];
	
	//---------Получаем доплнительные данные в массив
	//$ph = json_decode($array, true);
	//print_r($ph);
	
	//---------Получаем фото в массив
	if(empty($array['mainImage']) || empty($array['album'])){
		//----Обрабатываем старый модуль resource
		$photos = json_decode($array['resource'], true);
		//print_r($photos);
		foreach($photos as &$photo){
			if($photo['published'] == 1 && $photo['deleted'] == 0){
				$dirname = $_SERVER["DOCUMENT_ROOT"]."/tmp_photos/".$ID;
				if (file_exists($dirname)) {
					//echo "каталог $dirname существует";
				} else {
					if (!mkdir($dirname, 0777, true)) {
						die('Не удалось создать директорию...'.$dirname);
					}
				}
				$content = file_get_contents("https://krasnayapolyanaresort.ru/assets/resourceimages/".$ID."/".$photo['image']);
				file_put_contents($_SERVER["DOCUMENT_ROOT"]."/tmp_photos/".$ID."/".$photo['image'], $content);
				$arPhotos[] = $_SERVER["DOCUMENT_ROOT"]."/tmp_photos/".$ID."/".$photo['image'];
			}
		}
		
		
	}
	
	else{
		//----Обрабатываем новый модуль

		$photos = json_decode($array['album'], true);
		foreach($photos as &$photo){
			$dirname = $_SERVER["DOCUMENT_ROOT"]."/tmp_photos/".$ID;
			if (file_exists($dirname)) {
				//echo "каталог $dirname существует";
			} else {
				if (!mkdir($dirname, 0777, true)) {
					die('Не удалось создать директорию...'.$dirname);
				}
			}
			$content = file_get_contents("https://krasnayapolyanaresort.ru/assets/resourceimages/".$ID."/".$photo['filename']);
			file_put_contents($_SERVER["DOCUMENT_ROOT"]."/tmp_photos/".$ID."/".$photo['filename'], $content);
			$arPhotos[] = $_SERVER["DOCUMENT_ROOT"]."/tmp_photos/".$ID."/".$photo['filename'];
		}
	}
	$returnArray['photos'] = $arPhotos;
	return $returnArray;
}

function GetStars($VALUE){
	$ret = 44;
	switch($VALUE){
		case 0:
			$ret = 44;
		break;
		case 1:
			$ret = 39;
		break;
		case 2:
			$ret = 40;
		break;
		case 3:
			$ret = 41;
		break;
		case 4:
			$ret = 42;
		break;
		case 5:
			$ret = 43;
		break;
		default:
			$ret = 44;
		break;
	}
	return $ret;
}

function NormolizeHTML($Data){
	
}

function NormolizePosition($data){
	$JSONdata = html_entity_decode(html_entity_decode($data));
	$resultData = json_decode($JSONdata);
	$arrRes = explode(",", $resultData[0]);
	$result = $arrRes[0].".".$arrRes[1].",".$arrRes[2].".".$arrRes[3];
	return $result;
}
?>