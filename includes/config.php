<?
/**General */
define(LANG, "s1");
define(SERVER_NAME_HTTP, "https://test.uniqtrip.ru");
define (PAGE_LIMIT, 3);


/**Images */
//define (DEFAULT_PRICTURE, "https://easy-comp.ru/media/k2/items/cache/e9432fccf28a953514f077b86e5e657a_L.jpg");
define (DEFAULT_PRICTURE, SERVER_NAME_HTTP."/rest/img/dummy_mid.jpg");
define (DEFAULT_PRICTURE_SMALL, "/rest/img/dummy_small.jpg");
define (DEFAULT_PRICTURE_MID, "/rest/img/dummy_mid.jpg");
define (DEFAULT_PRICTURE_BIG, "/rest/img/dummy_big.jpg");
define (SMALL_IMG_W, 360);
define (SMALL_IMG_H, 240);
define (MID_IMG_W, 960);
define (MID_IMG_H, 720);
define (DEFAULT_PRICTURE, "https://easy-comp.ru/media/k2/items/cache/e9432fccf28a953514f077b86e5e657a_L.jpg");




define(DBG, "test");
define(TINKOFF_PAY_ID, 9);
/**Блок для бронирования*/
define(BOOKING, 7); 
define(BOOKING_ROOMS, 8);
define(BOOKING_OFFERS, 10);

/**Константы */
define(PARTNER, "catalog");
define(PRODUCT, "OFFER");



/**ERRORS CODES*/
/*Коды ошибок описывать тут
Пример:
define(ER_AUTH, 403);
*/


?>