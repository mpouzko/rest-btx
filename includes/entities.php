<?
include (dirname(__FILE__).'/functions.php');

//-----------Классы

class ENTITY {
	
	public $id;
	public $name;
	public $code;
    public $params = null;
	//($IBLOCK_ID, $IBLOCK_CODE = null, $ID, $CODE = null)
/*	function __construct ($IBLOCK_ID, $IBLOCK_CODE = null, $ID, $CODE = null){
		//print_r(LookupIBLOCK($IBLOCK_CODE));
		//die();
		if($IBLOCK_CODE){
			$filter1 = array('IBLOCK_CODE' => "acc");
			$filter1 = array('IBLOCK_ID' => LookupIBLOCK($IBLOCK_CODE)['ID']);
		}
		else
			//$filter1 = array('IBLOCK_ID' => $IBLOCK_ID);
			$filter1 = array('IBLOCK_ID' => LookupIBLOCK(null, $IBLOCK_ID)['ID']);


		if($CODE)
			$filter2 = array('CODE' => $CODE);
		else
			$filter2 = array('ID' => $ID);
		
		$filter = array_merge($filter1, $filter2); */



	function __construct ($ID, $CODE = null){
		if($CODE){
			$filter = array('CODE' => $CODE, "ACTIVE"=>"Y");
		}
		else
			$filter = array('ID' => $ID, "ACTIVE"=>"Y");
		
		\Bitrix\Main\Loader::includeModule('iblock');
		$arIblock = \Bitrix\Iblock\IblockTable::getList(array(
			'filter' => $filter,
		))->fetch();
		$this->code = $arIblock['CODE'];
		$this->name = $arIblock['NAME'];
		$this->id = $arIblock['ID'];
		$this->params = GetPropArray($arIblock['CODE']);
	}
	
	function GetItemsProp($data){
		if(!$data['offset'])
			$data['offset'] = 0;
		if(!$data['subcategoryId']){
			$filter = array('IBLOCK_ID' => $this->id, "ACTIVE"=>"Y");
		}
		else
			$filter = array('IBLOCK_ID' => $this->id, 'IBLOCK_SECTION_ID' => $data['subcategoryId'], "ACTIVE"=>"Y");
		$dbItem = \Bitrix\Iblock\ElementTable::getList(array(
			'select' => array('ID', 'IBLOCK_ID', 'NAME', 'CODE', 'IBLOCK_SECTION_ID'),
			'filter' => $filter,
			'limit' => PAGE_LIMIT,
			'offset' => $data['offset'],
			'order' => array('TIMESTAMP_X' => 'ASC')
		));
		//return $dbItem;
		while ($arItem = $dbItem->fetch()) {
			$i =0;
			$arItemRes=[];
			$arItemRes['id'] = $arItem['ID'];
			$arItemRes['name'] = $arItem['NAME'];
			$arItemRes['category_id'] = $arItem['IBLOCK_ID'];
			$arItemRes['subcategory_id'] = $arItem['IBLOCK_SECTION_ID'];
			
			$arItemRes['code'] = $arItem['CODE'];
			
			$dbProperty = \CIBlockElement::getProperty($arItem['IBLOCK_ID'], $arItem['ID'], array("sort", "asc"), $this->params);
			while ($arProperty = $dbProperty->GetNext()) {
				if ($arProperty['VALUE']) {
					if($arProperty['CODE'] == 'photos' || $arProperty['CODE'] == 'labels' || $arProperty['CODE'] == 'services'){
						if($arProperty['CODE'] == 'labels' || $arProperty['CODE'] == 'services'){
							$arItemRes['params'][$arProperty['CODE']][] = process_fields($arProperty);
						}
						elseif($i == 0){
							$arItemRes['params'][$arProperty['CODE']][] = process_fields($arProperty);
							$i++; 
						}
					}
					else
						$arItemRes['params'][$arProperty['CODE']] = process_fields($arProperty);
				}
			}
			if(!$arItemRes['params']['photos']) 
				$arItemRes['params']['photos'][] = ['small' => DEFAULT_PRICTURE, 'mid' => DEFAULT_PRICTURE];
			if($arItemRes['category_id'] == BOOKING){
				$arItemRes['params']['price'] = GetPrice($arItemRes['id'], $data, $arItemRes['name']);
				
				if($arItemRes['params']['price']){
					$arItemRes['params']['canbuy'] = true;
				}
				else {
					$arItemRes['params']['canbuy'] = false;
				}
				
			}
			else{
				$arItemRes['params']['price'] = 0;
				$arPrice = CPrice::GetBasePrice($arItemRes['id']);
				if($arPrice){
					if($arPrice['PRICE'] != "0.00" || !$arPrice['PRICE'] || $arPrice['PRICE'] != "0" ){
						$arItemRes['params']['price'] = (int)$arPrice['PRICE'];
						$arItemRes['params']['canbuy'] = true;
					}
					else{
						//$arItemRes['params']['price'] = 0;
						$arItemRes['params']['canbuy'] = false;						
					}
				}
				else{
					//unset($arItemRes['params']['price']);
					$arItemRes['params']['canbuy'] = false;
				}

				//$price = CPrice::GetBasePrice($arItemRes['id'])['PRICE'];
				//LogData("price", [$arItemRes,$price]);
				//if($price != "0.00" || !$price || $price != "0")
				//	$arItemRes['params']['price'] = CPrice::GetBasePrice($arItemRes['id'])['PRICE'];
				//else
				//$arItemRes['params']['test'] = CPrice::GetBasePrice($arItemRes['id']);
					//$arItemRes['params']['price'] = $price;
			}
			
			$arItems[] = $arItemRes;
		}

		$this->params = $arItems;
		return $arItems;
	}


	function GetBestPriceItems($params){
		if($params['categoryId'] == BOOKING){


			$result = [];
			$arSelect = Array(
				"IBLOCK_ID", 
				"ID", 
				"CODE", 
				"NAME", 
				"PROPERTY_114", //dateIn 			
				"PROPERTY_115", //dateOut
				"PROPERTY_76", //adults
				"PROPERTY_77", //children
				"PROPERTY_CML2_LINK",
				"PROPERTY_CML2_LINK.ID",
				"PROPERTY_CML2_LINK.CODE",		
				"PROPERTY_CML2_LINK.NAME",		
				"PROPERTY_CML2_LINK.IBLOCK_ID",	
				"PROPERTY_CML2_LINK.IBLOCK_SECTION_ID",	
				"PROPERTY_CML2_LINK.IBLOCK_SECTION_CODE",	
				"PROPERTY_CML2_LINK.IBLOCK_SECTION_NAME",	
				"PROPERTY_CML2_LINK.IBLOCK_CODE",	
				"PROPERTY_CML2_LINK.IBLOCK_NAME",	
				"CATALOG_PRICE_1",	
			);
			$arFilter = Array(
				"ACTIVE_DATE"=>"Y", 
				"ACTIVE"=>"Y", 
				"IBLOCK_ID" => 10, 
				'<=PROPERTY_114' => $params['dateOut'],
				'>=PROPERTY_115' => $params['dateIn'],
				'PROPERTY_76' => LookupSkuPerson('adults', $params['adults']),
				"PROPERTY_77" => LookupSkuPerson('children', $params['children']),
			);
			/*
			if(array_key_exists('term', $params))
				$arFilter = Array(
					"ACTIVE_DATE"=>"Y", 
					"ACTIVE"=>"Y", 
					"IBLOCK_ID" => 10, 
					array(
						"LOGIC" => "OR",
						array("PROPERTY_CML2_LINK.IBLOCK_SECTION_NAME" => "%".$params['term']."%"),
						//array("TAGS" => "%".$params['term']."%"),
					),	
					'<=PROPERTY_114' => $params['dateOut'],
					'>=PROPERTY_115' => $params['dateIn'],
					'PROPERTY_76' => LookupSkuPerson('adults', $params['adults']),
					"PROPERTY_77" => LookupSkuPerson('children', $params['children']),
				);			
			*/	
			//return $arFilter;
			$arGroupBy = ["NAME"];
			$res = CIBlockElement::GetList(Array('CATALOG_PRICE_1' => $params['sort']), $arFilter, $arGroupBy, Array(), $arSelect); //"nPageSize"=>PAGE_LIMIT, "iNumPage" => $params["offset"]
			//LogData("CIBlockElement::GetList", $params, DBG);
			$i = 0;
			while($ob = $res->GetNextElement())
			{
				/*
				if($i < PAGE_LIMIT * ($params["offset"]+1) && PAGE_LIMIT * $params["offset"] <= $i)
					$result[] = $ob->GetFields();
				if($i >= PAGE_LIMIT * ($params["offset"]+1))
					break;
				$i++;
				*/
				if($i <= (PAGE_LIMIT - 1 + $params["offset"]) && $params["offset"] <= $i)
					$result[] = $ob->GetFields();
				if($i >= ($params["offset"]+PAGE_LIMIT))
					break;
				$i++;				
			}
			$return = [];
			$Ent = new ENTITY($params['categoryId']);
			foreach($result as $hotelId){
				$arSelect = Array(
					"IBLOCK_ID", 
					"ID", 
					"CODE", 
					"NAME", 
					"PROPERTY_114", //dateIn 			
					"PROPERTY_115", //dateOut
					"PROPERTY_76", //adults
					"PROPERTY_77", //children
					"PROPERTY_CML2_LINK",
					"PROPERTY_CML2_LINK.ID",
					"PROPERTY_CML2_LINK.CODE",		
					"PROPERTY_CML2_LINK.NAME",		
					"PROPERTY_CML2_LINK.IBLOCK_ID",	
					"PROPERTY_CML2_LINK.IBLOCK_SECTION_ID",	
					"PROPERTY_CML2_LINK.IBLOCK_SECTION_CODE",	
					"PROPERTY_CML2_LINK.IBLOCK_SECTION_NAME",	
					"PROPERTY_CML2_LINK.IBLOCK_CODE",	
					"PROPERTY_CML2_LINK.IBLOCK_NAME",	
					"CATALOG_PRICE_1",	
				);
				$arFilter = Array(
					"ACTIVE_DATE"=>"Y", 
					"ACTIVE"=>"Y", 
					"IBLOCK_ID" => BOOKING_OFFERS, 
					"NAME" => $hotelId['NAME'], 
					'<=PROPERTY_114' => $params['dateOut'],
					'>=PROPERTY_115' => $params['dateIn'],
					'PROPERTY_76' => LookupSkuPerson('adults', $params['adults']),
					"PROPERTY_77" => LookupSkuPerson('children', $params['children']),
				); 
				$arGroupBy = false;
				$resData = CIBlockElement::GetList(Array('CATALOG_PRICE_1' => $params['sort']), $arFilter, $arGroupBy, Array("nPageSize"=>1), $arSelect);
				while($obData = $resData->GetNextElement())
				{
					$data = $obData->GetFields();
					$arItems = $Ent->GetItemPropByID($hotelId['NAME']);
					//$arItems[0]['1testprice'] =  $data['CATALOG_PRICE_1'];
					$arItems[0]['params']['price'] =  $data['CATALOG_PRICE_1'];
					$arItems[0]['params']['productid'] =  $data['PROPERTY_CML2_LINK_ID']; //$data['ID']
					//$arItems[0]['test'] = $data;
					$return[] = $arItems[0];
				}			
			}
			return $return;
		}
		else{

			$result = [];
			$arSelect = Array(
				"IBLOCK_ID", 
				"ID", 
				"CODE", 
				"NAME", 
				//"PROPERTY_114", //dateIn 		 	
				//"PROPERTY_115", //dateOut
				//"PROPERTY_76", //adults
				//"PROPERTY_77", //children
				"PROPERTY_CML2_LINK",
				"PROPERTY_CML2_LINK.ID",
				"PROPERTY_CML2_LINK.CODE",		
				"PROPERTY_CML2_LINK.NAME",		
				"PROPERTY_CML2_LINK.IBLOCK_ID",	
				"PROPERTY_CML2_LINK.IBLOCK_SECTION_ID",	
				"PROPERTY_CML2_LINK.IBLOCK_SECTION_CODE",	
				"PROPERTY_CML2_LINK.IBLOCK_SECTION_NAME",	
				"PROPERTY_CML2_LINK.IBLOCK_CODE",	
				"PROPERTY_CML2_LINK.IBLOCK_NAME",	
				"CATALOG_PRICE_1",	
			);
			$arFilter = Array(
				"ACTIVE_DATE"=>"Y", 
				"ACTIVE"=>"Y", 
				"PROPERTY_CML2_LINK.ACTIVE"=>"Y", 
				"IBLOCK_ID" => LookupIBLOCK_OFFER($params['categoryId'])[1], 
			);
			//LogData("LookupIBLOCK_OFFER", $arFilter);
			$arGroupBy = false;
			$res = CIBlockElement::GetList(Array('CATALOG_PRICE_1' => $params["sort"]), $arFilter, $arGroupBy, Array(), $arSelect); //"nPageSize"=>PAGE_LIMIT, "iNumPage" => $params["offset"]
			while($ob = $res->GetNextElement())
			{
					$result[] = $ob->GetFields();
			}
			$return = [];
			$Ent = new ENTITY($params['categoryId']);
			$i = 0;
			$arTmp = [];
			foreach($result as $hotelId){
				if(!in_array($hotelId['PROPERTY_CML2_LINK_VALUE'], $arTmp)){
					$arTmp[] = $hotelId['PROPERTY_CML2_LINK_VALUE'];

					//if($i < PAGE_LIMIT * ($params["offset"]+1) && PAGE_LIMIT * $params["offset"] <= $i){
					if($i <= (PAGE_LIMIT - 1 + $params["offset"]) && $params["offset"] <= $i){
						$tmp = $Ent->GetItemPropByID($hotelId['PROPERTY_CML2_LINK_VALUE']);
						$tmp[0]['params']['price'] = (int)$hotelId['CATALOG_PRICE_1'];
						$tmp[0]['params']['productid'] = $hotelId['ID'];
						$return[] = $tmp[0];
					}
					//if($i >= PAGE_LIMIT * ($params["offset"]+1))
					if($i >= ($params["offset"]+PAGE_LIMIT))
						break;
					$i++;					
				}
				
			}
			return $return;
			
		}
	}	 


	/********не используется */
	function GetBestPriceItems_old($params){
		if(!$params['offset'])
			$params['offset'] = 0;
		$result = [];
		$mysqli = new mysqli("localhost", "root", "QZWxxv3GRN", "test_bitrix");
		if ($mysqli->connect_errno) {
			echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
		}

		$query = "
		select 
			min(PRICE.PRICE) as minprice
			,MAIN.PROPERTY_80 as productid
			,PARTNER.IBLOCK_ELEMENT_ID as partner
			,PARTNER.PROPERTY_93 as cat
		from `b_iblock_element_prop_s10` MAIN
			left join `b_catalog_price` PRICE on MAIN.IBLOCK_ELEMENT_ID = PRICE.PRODUCT_ID
			left join `b_iblock_element` PROD on MAIN.PROPERTY_80 = PROD.ID
			left join `b_iblock_element_prop_s7` PARTNER on PROD.IBLOCK_SECTION_ID = PARTNER.PROPERTY_93
		where 1=1
        and MAIN.PROPERTY_114 <= '".$params['dateOut']."'
		and MAIN.PROPERTY_115 >= '".$params['dateIn']."'
		and MAIN.PROPERTY_76 >= ".LookupSkuPerson('adults', $params['adults'])."
		#and MAIN.PROPERTY_77 >= ".LookupSkuPerson('children', $params['children'])."
		group by PARTNER.IBLOCK_ELEMENT_ID
		order by PRICE.PRICE asc
		limit ".($params['offset']*PAGE_LIMIT).", ".($params['offset']+1)*PAGE_LIMIT;
		//print_r($query);
		//and MAIN.PROPERTY_114 >= '".$params['dateIn']."'
		//and MAIN.PROPERTY_115 <= '".$params['dateOut']."'

		//die();
		$res = $mysqli->query($query);
		if (!$res){
			echo "Не удалось (" . $mysqli->errno . ") " . $mysqli->error;
		}
		else{
			while ($row = $res->fetch_assoc()) {
				//return $row;
				$query = "select IBLOCK_SECTION_ID, IBLOCK_ID, NAME, CODE from `b_iblock_element`
				where IBLOCK_ID = ".$this->id."
				and ID=".$row['partner'];

				$res2 = $mysqli->query($query);
				$row2 = $res2->fetch_assoc();



				$i =0;
				$arItemRes=[];
				$arItemRes['id'] = $row['partner'];
				$arItemRes['name'] = $row2['NAME'];//$arItem['NAME'];
				$arItemRes['category_id'] = $this->id;//$arItem['IBLOCK_ID'];
				$arItemRes['subcategory_id'] = $row2['IBLOCK_SECTION_ID'];//$arItem['IBLOCK_SECTION_ID'];
				$arItemRes['code'] = $row2['CODE']; //$arItem['CODE'];
				//$arItemRes['off'] = (((int)$params['offset']+1)*2);
				//$arItemRes['off2'] = $params['offset'];
				
				$dbProperty = \CIBlockElement::getProperty($this->id, $row['partner'], array("sort", "asc"), $this->params);
				while ($arProperty = $dbProperty->GetNext()) {
					if ($arProperty['VALUE']) {
						if($arProperty['CODE'] == 'photos' || $arProperty['CODE'] == 'labels' || $arProperty['CODE'] == 'services'){
							if($arProperty['CODE'] == 'labels' || $arProperty['CODE'] == 'services'){
								$arItemRes['params'][$arProperty['CODE']][] = process_fields($arProperty);
							}
							elseif($i == 0){
								$arItemRes['params'][$arProperty['CODE']][] = process_fields($arProperty);
								$i++; 
							}
						}
						else
							$arItemRes['params'][$arProperty['CODE']] = process_fields($arProperty);
					}
				}
	
				if($arItemRes['category_id'] == BOOKING){
					$arItemRes['params']['price'] = $row['minprice'];//GetPrice($data, $arItemRes['id'], $arItemRes['name']);
					LogData("Тут цена ", [$row['productid'], $params, $dateIn], DBG);
					$arItemRes['params']['price2'] = getPrice($row['productid'], $params, $dateIn);
					$arItemRes['params']['productid'] = $row['productid'];
					
					if($arItemRes['params']['price']){
						$arItemRes['params']['canbuy'] = true;
					}
					else {
						$arItemRes['params']['canbuy'] = false;
					}
					
				}
				else{
					$arItemRes['params']['canbuy'] = false;
					$arItemRes['params']['price'] = "123"; //Статика
				}
				$arItems[] = $arItemRes;
			}
			return $arItems;
		}
	}

	
	function GetItemPropByID($ID = null, $partnerCode = null){
		//partnerCode: "polyet_na_paraplane"
		//categoryCode: "rides"		
		$filter = array('IBLOCK_ID' => $this->id, "ACTIVE"=>"Y");
		if(!$partnerCode)
			$filter = array('ID' => $ID, "ACTIVE"=>"Y");
			
		if(!$ID)
			$filter = array('CODE' => $partnerCode, "ACTIVE"=>"Y");
		$dbItem = \Bitrix\Iblock\ElementTable::getList(array(
			'select' => array('ID', 'IBLOCK_ID', 'NAME', 'CODE'),
			'filter' => $filter, //array('IBLOCK_ID' => $this->id, 'ID' => $ID),
			'limit' => 1,
			//'offset' => $offset,
			//'order' => array('TIMESTAMP_X' => 'ASC')
		));
		while ($arItem = $dbItem->fetch()) {
			$i =0;
			$arItemRes=[];
			$arItemRes['id'] = $arItem['ID'];
			$arItemRes['name'] = $arItem['NAME'];
			$arItemRes['category_id'] = $arItem['IBLOCK_ID'];
			$arItemRes['code'] = $arItem['CODE'];
			$dbProperty = \CIBlockElement::getProperty($arItem['IBLOCK_ID'], $arItem['ID'], array("sort", "asc"), $this->params);
			while ($arProperty = $dbProperty->GetNext()) {
				if ($arProperty['VALUE']) {
					if($arProperty['CODE'] == 'photos' || $arProperty['CODE'] == 'labels' || $arProperty['CODE'] == 'services'){
						if($arProperty['CODE'] == 'labels' || $arProperty['CODE'] == 'services'){
							$arItemRes['params'][$arProperty['CODE']][] = process_fields($arProperty);
						}
						elseif($i == 0){
							$arItemRes['params'][$arProperty['CODE']][] = process_fields($arProperty);
							$i++; 
						}
					}
					elseif($arProperty['CODE'] == 'terms' || $arProperty['CODE'] == 'note' || $arProperty['CODE'] == 'services_desc'){
						$arItemRes['params'][$arProperty['CODE']] = process_fields($arProperty);
						$arItemRes['params'][$arProperty['CODE'].'_title'] = $arProperty['NAME'];
					}
					else{
						$arItemRes['params'][$arProperty['CODE']] = process_fields($arProperty);
					}					
				}
			}
			$arItems[] = $arItemRes;
		}
		return $arItems;
	}	


	function GetProductsByPartnerID($params){
		
		$ID = $params['partnerId'];
		$CODE = $params['partnerCode'];
/*
		  "partnerId" => 629,
		  "categoryId" => 7,
		  "dateIn" => "2020-01-01",
		  "dateOut" => "2020-01-01",
		  "adults" => 2,
		  "children" => 0
*/			
			

		if($CODE)
			$filter = ['IBLOCK_ID' => $this->id, 'CODE' => $CODE, "ACTIVE"=>"Y"];
		else
			$filter = ['IBLOCK_ID' => $this->id, 'ID' => $ID, "ACTIVE"=>"Y"];

		$dbItem = \Bitrix\Iblock\ElementTable::getList(array(
			'select' => array('ID', 'IBLOCK_ID', 'NAME', 'IBLOCK_SECTION_ID', 'CODE'),
			'filter' => $filter,
			//'limit' => PAGE_LIMIT,
			//'offset' => $offset,			
			//'order' => array('TIMESTAMP_X' => 'ASC')
		));
		//return $filter;
		while ($arItem = $dbItem->fetch()) {
			$i = 0;
			$arItemRes=[];
			$arItemRes['id'] = $arItem['ID'];
			$arItemRes['name'] = $arItem['NAME'];
			$arItemRes['category_id'] = $arItem['IBLOCK_ID'];
			$arItemRes['subcategory_id'] = $arItem['IBLOCK_SECTION_ID'];
			
			$arItemRes['code'] = $arItem['CODE'];

			//получаем продукты в случае если нет свойства ссылки на категорию продуктов
			if($arItem['IBLOCK_ID'] == BOOKING){

				$dbProperty = \CIBlockElement::getProperty($arItem['IBLOCK_ID'], $arItem['ID'], array("sort", "asc"), $this->params);
				while ($arProperty = $dbProperty->GetNext()) {
					if ($arProperty['VALUE']) {
						if($arProperty['CODE'] == 'photos' || $arProperty['CODE'] == 'labels' || $arProperty['CODE'] == 'services'){
							if($arProperty['CODE'] == 'labels' || $arProperty['CODE'] == 'services'){
								$arItemRes['params'][$arProperty['CODE']][] = process_fields($arProperty);
							}
							elseif($i == 0){
								$arItemRes['params'][$arProperty['CODE']][] = process_fields($arProperty);
								$i++; 
							}
						}
						elseif($arProperty['CODE'] == 'products'){
							//$arItemRes['params'][$arProperty['CODE']] = 'good';
							$products = GetProducts(null, $arItemRes['category_id'], $arProperty, $arItem['IBLOCK_ID'], $arItem['IBLOCK_SECTION_ID'], $params);
							if($products){
								//if(!$products['services_desc']) 
									//$products['services_desc'] = " ";								
								$arItemRes['params'][$arProperty['CODE']] = $products;
							}
						}
						else
							$arItemRes['params'][$arProperty['CODE']] = process_fields($arProperty);
						
					}
					
				}
				//$arItemRes[0]['params']['products']['services_desc'] = "123";
			}
			else{
				$products = GetProducts($arItemRes['id'], $arItemRes['category_id'], $arProperty, $arItem['IBLOCK_ID'], $arItem['IBLOCK_SECTION_ID'], $params);
				if($products){
					if(!$products[0]['services_desc']) 
						$products[0]['services_desc'] = " ";
					return $products;
				}
			}
			//$arItemRes[] = ['services_desc' => " "];

			$arItems[] = $arItemRes;
			//$arItems['services_desc'] = " ";
		}
		//$arItems[0]['params']['products'][] = "123";
		//if(!$arItems[0]['params']['products']['services_desc'])
			//$arItems[0]['params']['products']['services_desc'] = " ";
		//return $arItems[0]['params']['products']
		return $arItems[0]['params']['products'];
	}	
	
	//используется для импорта
	function GetItemByPriceName($NAME){

		$arFilter = Array(
		 "IBLOCK_ID"=>BOOKING, 
		 "PROPERTY_pricename" => $NAME,
		 "ACTIVE"=>"Y"
		 );
		// print_r($arFilter);
		$res = CIBlockElement::GetList(Array(), $arFilter);
		$cnt = $res->SelectedRowsCount();
		if($cnt > 1 || $cnt == 0){
			echo "нет такого отеля или найдено $cnt значений";
			return null;
		}
		
		$ar_fields = $res->GetNext();
		$roomsCatalog = \CIBlockElement::getProperty($ar_fields['IBLOCK_ID'], $ar_fields['ID'], array("sort", "asc"), array('CODE' => 'products'));			
		$arProperty = $roomsCatalog->GetNext();
		
			$result = [
				'id' => $ar_fields['ID'],
				'name' => $ar_fields['NAME'],
				'categoryid' => $ar_fields['IBLOCK_ID'],
				'code' => $ar_fields['CODE'],
				'categoryname' => $ar_fields['IBLOCK_NAME'],
				'pricename' => $NAME,
				'catalogid' => $arProperty['VALUE']
			];
		return $result;		


die();

		while ($arItem = $dbItem->fetch()) {
			$i =0;
			$arItemRes=[];
			$arItemRes['id'] = $arItem['ID'];
			$arItemRes['name'] = $arItem['NAME'];
			$arItemRes['category_id'] = $arItem['IBLOCK_ID'];
			$arItemRes['code'] = $arItem['CODE'];
			$dbProperty = \CIBlockElement::getProperty($arItem['IBLOCK_ID'], $arItem['ID'], array("sort", "asc"), $this->params);
			while ($arProperty = $dbProperty->GetNext()) {
				if ($arProperty['VALUE']) {
					if($arProperty['CODE'] == 'photos' || $arProperty['CODE'] == 'labels' || $arProperty['CODE'] == 'services'){
						if($arProperty['CODE'] == 'labels' || $arProperty['CODE'] == 'services'){
							$arItemRes['params'][$arProperty['CODE']][] = process_fields($arProperty);
						}
						elseif($i == 0){
							$arItemRes['params'][$arProperty['CODE']][] = process_fields($arProperty);
							$i++; 
						}
					}
					else
						$arItemRes['params'][$arProperty['CODE']] = process_fields($arProperty);
				}
			}
			$arItems[] = $arItemRes;
		}
		return $arItems;
	}	

	function GetPhotos($partnerId, $partnerCode = null){

		$arItemRes=[];
		if($partnerCode)
			$partner = LookupItem(null, $partnerCode, $this->id);
		else
			$partner['ID'] = $partnerId;
		$dbProperty = \CIBlockElement::getProperty($this->id, $partner['ID'], array("sort", "asc"), array('CODE' => 'photos'));
		while ($arProperty = $dbProperty->GetNext()) {
			$img_path_small = CFile::ResizeImageGet($arProperty['VALUE'], array('width'=>SMALL_IMG_W, 'height'=>SMALL_IMG_H), BX_RESIZE_IMAGE_EXACT, true);
			$img_path_mid = CFile::ResizeImageGet($arProperty['VALUE'], array('width'=>MID_IMG_W, 'height'=>MID_IMG_H), BX_RESIZE_IMAGE_EXACT, true);
			$arPhoto['small'] = SERVER_NAME_HTTP.$img_path_small['src'];
			$arPhoto['mid'] = SERVER_NAME_HTTP.$img_path_mid['src'];
			$arItemRes[] =  $arPhoto;
		}
		return $arItemRes;
	}	

	//-----------не используется - функция кошка
	function test_function($params){
		/*$params = array(
			"offset" => 0,
			"categoryId" => 7,
			"subcategoryId" => 0,
			"dateIn" => "2020-01-01",
			"dateOut" => "2020-01-01",
			"adults" => 2,
			"children" => 0,
			"time" => "10:10:10",
		  );*/
/*
          and MAIN.PROPERTY_114 <= '".$params['dateOut']."'
		and MAIN.PROPERTY_115 >= '".$params['dateIn']."'
		and MAIN.PROPERTY_76 >= ".LookupSkuPerson('adults', $params['adults'])."
		#and MAIN.PROPERTY_77 >= ".LookupSkuPerson('children', $params['children'])."
  
 */

		//return $Items;
		$result = [];
		$arSelect = Array(
			"IBLOCK_ID", 
			"ID", 
			"CODE", 
			"NAME", 
			"PROPERTY_114", //dateIn 			
			"PROPERTY_115", //dateOut
			"PROPERTY_76", //adults
			"PROPERTY_77", //children
			"PROPERTY_CML2_LINK",
			"PROPERTY_CML2_LINK.ID",
			"PROPERTY_CML2_LINK.CODE",		
			"PROPERTY_CML2_LINK.NAME",		
			"PROPERTY_CML2_LINK.IBLOCK_ID",	
			"PROPERTY_CML2_LINK.IBLOCK_SECTION_ID",	
			"PROPERTY_CML2_LINK.IBLOCK_SECTION_CODE",	
			"PROPERTY_CML2_LINK.IBLOCK_SECTION_NAME",	
			"PROPERTY_CML2_LINK.IBLOCK_CODE",	
			"PROPERTY_CML2_LINK.IBLOCK_NAME",	
			"CATALOG_PRICE_1",	
		); //, "PROPERTY_CML2_LINK"  CML2_LINK
		$arFilter = Array(
			"ACTIVE_DATE"=>"Y", 
			"ACTIVE"=>"Y", 
			"IBLOCK_ID" => 10, 
			//"NAME" => 629, 
			'<=PROPERTY_114' => $params['dateOut'], //0101
			'>=PROPERTY_115' => $params['dateIn'],
			'PROPERTY_76' => LookupSkuPerson('adults', $params['adults']),
			"PROPERTY_77" => LookupSkuPerson('children', $params['children']),
		); //'PROPERTY_CML2_LINK.ID' => 10557
		$arGroupBy = ["NAME"];
		//$arGroupBy = false;
		$res = CIBlockElement::GetList(Array('CATALOG_PRICE_1' => 'ASC'), $arFilter, $arGroupBy, Array("nPageSize"=>PAGE_LIMIT), $arSelect);
		while($ob = $res->GetNextElement())
		{
		  	$result[] = $ob->GetFields();
			//$return[] = $result;
		}
		//return $result;		
		$return = [];
		$Ent = new ENTITY($params['categoryId']);
		foreach($result as $hotelId){
			//print_r($hotelId['NAME']);
			
			$arSelect = Array(
				"IBLOCK_ID", 
				"ID", 
				"CODE", 
				"NAME", 
				"PROPERTY_114", //dateIn 			
				"PROPERTY_115", //dateOut
				"PROPERTY_76", //adults
				"PROPERTY_77", //children
				"PROPERTY_CML2_LINK",
				"PROPERTY_CML2_LINK.ID",
				"PROPERTY_CML2_LINK.CODE",		
				"PROPERTY_CML2_LINK.NAME",		
				"PROPERTY_CML2_LINK.IBLOCK_ID",	
				"PROPERTY_CML2_LINK.IBLOCK_SECTION_ID",	
				"PROPERTY_CML2_LINK.IBLOCK_SECTION_CODE",	
				"PROPERTY_CML2_LINK.IBLOCK_SECTION_NAME",	
				"PROPERTY_CML2_LINK.IBLOCK_CODE",	
				"PROPERTY_CML2_LINK.IBLOCK_NAME",	
				"CATALOG_PRICE_1",	
			); //, "PROPERTY_CML2_LINK"  CML2_LINK
			$arFilter = Array(
				"ACTIVE_DATE"=>"Y", 
				"ACTIVE"=>"Y", 
				"IBLOCK_ID" => 10, 
				"NAME" => $hotelId['NAME'], 
				'<=PROPERTY_114' => $params['dateOut'], //0101
				'>=PROPERTY_115' => $params['dateIn'],
				'PROPERTY_76' => LookupSkuPerson('adults', $params['adults']),
				"PROPERTY_77" => LookupSkuPerson('children', $params['children']),
			); //'PROPERTY_CML2_LINK.ID' => 10557
			//$arGroupBy = ["NAME"];
			$arGroupBy = false;
			$resData = CIBlockElement::GetList(Array('CATALOG_PRICE_1' => 'ASC'), $arFilter, $arGroupBy, Array("nPageSize"=>1), $arSelect);
			
			while($obData = $resData->GetNextElement())
			{
				$data = $obData->GetFields();
				$arItems = $Ent->GetItemPropByID($hotelId['NAME']);
				$arItems[0]['params']['price'] =  $data['CATALOG_PRICE_1'];
				$arItems[0]['params']['productid'] =  $data['ID'];
				$return[] = $arItems[0];
				/*
				  $return[] = [
					  'id' => $data['ID'],
					  'name' => $data['PROPERTY_CML2_LINK_NAME'],
					  'code' => $data['PROPERTY_CML2_LINK_CODE'],
					  'roomId' => $data['PROPERTY_CML2_LINK_ID'],
					  //'partnerName' => $data['ID'],
					  'partnerId' => $data['NAME'],
					  'price' => $data['CATALOG_PRICE_1'],


				  ];
				  //echo "1111";
				  //print_r($obData->GetFields());
				//$return[] = $result;
				*/
			}			
		}
		return $return;





/***************************************** */		
		//return $Items;
		/*
		$result = [];
		foreach ($Items as $Item){
			$arSelect = Array("ID", "NAME", "IBLOCK_SECTION_ID");
			$arFilter = Array("IBLOCK_ID"=> 8, "ID" => $Item['parentId']); //, "ACTIVE"=>"Y"
			$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
			while($ob = $res->GetNextElement())
			{
				$arFields = $ob->GetFields();
				
				$arFilterP = Array("IBLOCK_ID"=>$this->id, "PROPERTY_products"=>$arFields['IBLOCK_SECTION_ID']); //, "ACTIVE"=>"Y"
				$arSelectP = Array("ID", "NAME", "IBLOCK_SECTION_ID", "IBLOCK_ID", 'CODE');
				$partners = CIBlockElement::GetList(Array(), $arFilterP, false, Array("nPageSize"=>50), $arSelectP);
				
				while($obP = $partners->GetNextElement())
				{
					$arFieldsPartner = $obP->GetFields();
					$i =0;
					$arItemRes=[];
					$arItemRes['id'] = $arFieldsPartner['ID'];
					$arItemRes['name'] = $arFieldsPartner['NAME'];
					$arItemRes['category_id'] = $arFieldsPartner['IBLOCK_ID'];
					$arItemRes['subcategory_id'] = $arFieldsPartner['IBLOCK_SECTION_ID'];
					
					$arItemRes['code'] = $arFieldsPartner['CODE'];					
					
					$dbProperty = \CIBlockElement::getProperty($arFieldsPartner['IBLOCK_ID'], $arFieldsPartner['ID'], array("sort", "asc"), $this->params);
					while ($arProperty = $dbProperty->GetNext()) {
						if ($arProperty['VALUE']) {
							if($arProperty['CODE'] == 'photos' || $arProperty['CODE'] == 'labels' || $arProperty['CODE'] == 'services'){
								if($arProperty['CODE'] == 'labels' || $arProperty['CODE'] == 'services'){
									$arItemRes['params'][$arProperty['CODE']][] = process_fields($arProperty);
								}
								elseif($i == 0){
									$arItemRes['params'][$arProperty['CODE']][] = process_fields($arProperty);
									$i++; 
								}
							}
							else
								$arItemRes['params'][$arProperty['CODE']] = process_fields($arProperty);
						}
					}					
					
					if($arItemRes['category_id'] == BOOKING){
						//$arItemRes['params']['price'] = GetPrice($data, $arItemRes['id'], $arItemRes['name']);
						$arItemRes['params']['price'] = $Item['price'];
						$arItemRes['params']['product_id'] = $Item['parentId'];
						if($arItemRes['params']['price']){
							$arItemRes['params']['canbuy'] = true;
						}
						else {
							$arItemRes['params']['canbuy'] = false;
						}
					}
					else{
						$arItemRes['params']['canbuy'] = false;
						$arItemRes['params']['price'] = "123";
					}
					$arItems[] = $arItemRes;
					$this->params = $arItems;					
				}
			}			
		}
		return $arItems;
		*/
	}


    function __destruct() {
        //print "Уничтожается " . __CLASS__  . "\n";
    }	


}


//Пока так
class SKU {
	
	public $id;
	public $name;
	public $code;
	public $price;
	public $type;

	function __construct ($ID, $CODE = null){

	}

	function GetTopDiscount(){
		$result = [];
		$arSelect = Array(
			"IBLOCK_ID", 
			"ID", 
			"CODE", 
			"NAME", 
			"DATE_ACTIVE_FROM", 
			"PROPERTY_discount", 
			"PREVIEW_PICTURE", 
			"PREVIEW_TEXT", 
			"PROPERTY_CML2_LINK",
			"PROPERTY_CML2_LINK.ID",
			"PROPERTY_CML2_LINK.CODE",		
			"PROPERTY_CML2_LINK.IBLOCK_ID",	
			"PROPERTY_CML2_LINK.IBLOCK_CODE",	
			"PROPERTY_CML2_LINK.IBLOCK_NAME",	
		); //, "PROPERTY_CML2_LINK"  CML2_LINK
		$arFilter = Array(
			"ACTIVE_DATE"=>"Y", 
			"ACTIVE"=>"Y", 
			"PROPERTY_DISCOUNT_VALUE" => "Да", 
			"IBLOCK_ID" => [17,23,26,14,15,27,19,24] //Статика все блоки с ТП кроме проживания
		);
		$res = CIBlockElement::GetList(Array("RAND"=>"ASC"), $arFilter, false, Array("nPageSize"=>PAGE_LIMIT), $arSelect);
		while($ob = $res->GetNextElement())
		{
		  	$result = $ob->GetFields();
			$return[] = [
				"id" => $result['ID'],
				"name" => $result['NAME'],
				"code" => $result['PROPERTY_CML2_LINK_CODE'],
				"parentId" => $result['PROPERTY_CML2_LINK_IBLOCK_ID'],
				"parentName" => $result['PROPERTY_CML2_LINK_IBLOCK_NAME'],
				"parentCode" => $result['PROPERTY_CML2_LINK_IBLOCK_CODE'],
				"price" => GetPrice($result['ID'], null),
				"services_desc" => $result['PREVIEW_TEXT'],
				"photos" =>  [GetPhotoById($result['PREVIEW_PICTURE'])],
				//"test" => $result,
			];
		}
		return $return;
	}

}

?>


