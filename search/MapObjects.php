<?

function MapObjects($params){
	//$params = $_REQUEST['params'];
	if($_REQUEST[DBG] == 1)	{
		$params = array(
			"partnerId" => 376,
			'categoryId' => [7,9],
		);
	}
	//print_r(json_encode($params));

	$Partners = GetPartners($params['categoryId'], $params['subcategoryId']);
	return $Partners;
}
//print_r(json_encode(GetLable(415)));



function GetPartners($categoryId, $subcategoryId){
	
	$arResult = array();

	$arSelect = Array("ID", "NAME", "IBLOCK_SECTION_ID", "IBLOCK_CODE", "CODE");

	$arFilter = Array("IBLOCK_ID"=> [$categoryId], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "IBLOCK_SECTION_ID" => $subcategoryId);
		
	//---------Получаем элементы
	$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
	
	//---------Перебираем все элементы
	
	while($ob = $res->GetNextElement())
	{
		$arFields = $ob->GetFields();
		$arProp = GetProperty($arFields['ID']);
		//---------формируем результирующий массив
		$price = rand(5000, 100000);
		$oldprice = $price+3000;
		array_push($arResult, 
							array(
									"partnerId" => $arFields['ID'], 
									"productId" => 393, //Не готово
									"title" => $arFields['NAME'], 
									"position" => $arProp['position'], //Не готово
									"category" => $arFields['IBLOCK_CODE'], 
								)
					);
	}
	//return $arFields;
	return $arResult;

}

function GetProperty($ID){
		
		
	
		//---------Получаем свойства
		$prop=CIBlockElement::GetByID($ID)->GetNextElement()->GetProperties();
		$arPhoto = array('small' => array(),'mid' => array());
		$arProp=[];
		//---------Перебираем все свойства
		foreach ($prop as &$prop_value) {
			//---------Услови если фото
			if ($prop_value['CODE'] == 'photos'){
				
				foreach ($prop_value['VALUE'] as &$photo_value) {
					$img_path_small = CFile::ResizeImageGet($photo_value, array('width'=>360, 'height'=>360), BX_RESIZE_IMAGE_PROPORTIONAL, true);
					$img_path_mid = CFile::ResizeImageGet($photo_value, array('width'=>720, 'height'=>720), BX_RESIZE_IMAGE_PROPORTIONAL, true);
					array_push($arPhoto['small'], $img_path_small['src']);
					array_push($arPhoto['mid'], $img_path_mid['src']);
				}
			}
			//---------создаем массив со свойствами
				//$arProp[] = array($prop_value['CODE'], $prop_value['VALUE']);
			$arProp[$prop_value['CODE']] = $prop_value['VALUE'];
		}
		foreach ($arProp['labels'] as &$prop_value) {
			$labels = GetLable($prop_value);
		}
		$arProp = array_filter($arProp);
		$arProp['labels'] = $labels;
		$arProp['images'] = $arPhoto;
		return $arProp; //$arPr $arProp
}

function GetLable($ID){
		//---------Получаем список лейблов
	$arResult = array();
	$arOrder = Array("sort"=>"ASC");
	$arSelect = Array("ID", "NAME", "IBLOCK_CODE", "CODE");

	$arFilter = Array("IBLOCK_ID"=> 12, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "IBLOCK_SECTION_ID" => "", "ID" => $ID);		
		$res = CIBlockElement::GetList($arOrder, $arFilter, false, Array("nPageSize"=>50), $arSelect);
		while($ob = $res->GetNextElement())
		{
			$arFields = $ob->GetFields();
			$prop=CIBlockElement::GetByID($ID)->GetNextElement()->GetProperties();
			array_push($arResult, array('title' => $prop['title']['VALUE'], 'class' => $prop['class']['VALUE']));
		}	
		return $arResult;
}
?>
