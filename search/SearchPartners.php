<?php

function SearchPartners($params){
	if($_REQUEST[DBG] == 1 || $_REQUEST[DBG] == 2 || $params['test'])	{	
		$params = array(
			//"categoryId" => 9,
			//"term" => "сочи",
			//"offset" => 6
			"categoryId" => "7",
			"dateIn" => "2019-12-28",
			"dateOut" => "2020-01-03",
			"adults" => 2,
			"children" => 0,
			"time" => "10:00",
			"term" => "сочи"			
		  );
	}

	if(!$params['term'])
		return null;

	if(!$params['categoryId'])
		$arFilter = Array(
			"ACTIVE_DATE"=>"Y", 
			"ACTIVE"=>"Y", 
			array(
				"LOGIC" => "OR",
				array("NAME" => "%".$params['term']."%"),
				array("TAGS" => "%".$params['term']."%"),
			),				
		);
	else
		$arFilter = Array(
			"IBLOCK_ID"=>$params['categoryId'], 
			"ACTIVE_DATE"=>"Y", 
			"ACTIVE"=>"Y", 
			array(
				"LOGIC" => "OR",
				array("NAME" => "%".$params['term']."%"),
				array("TAGS" => "%".$params['term']."%"),
			),				
		);

	$arResult = array();

	$arSelect = Array("ID", "NAME", "IBLOCK_NAME", "IBLOCK_SECTION_ID", "IBLOCK_ID", "IBLOCK_CODE", "CODE");
	//$arSelect = Array();
	
	$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>PAGE_LIMIT, "iNumPage" => $params["offset"]/PAGE_LIMIT), $arSelect);
	//$count = $res->nSelectedCount;

	while($ob = $res->GetNextElement())
	{
		//print_r($ob);
		$arFields = $ob->GetFields();
			//print_r($arFields);
		//while ($arItem = $dbItem->fetch()) {
			$i =0;
			$arItemRes=[];
			$arItemRes['id'] = $arFields['ID'];
			$arItemRes['name'] = $arFields['NAME'];
			$arItemRes['category_id'] = $arFields['IBLOCK_ID'];
			$arItemRes['subcategory_id'] = $arFields['IBLOCK_SECTION_ID'];
			//$arItemRes['test'] = $arFields;
			$arItemRes['code'] = $arFields['CODE'];
			
			$dbProperty = \CIBlockElement::getProperty($arFields['IBLOCK_ID'], $arFields['ID'], array("sort", "asc"), GetPropArray($arFields['CODE']));
			while ($arProperty = $dbProperty->GetNext()) {
				//print_r($arProperty);
				if ($arProperty['VALUE']) {
					if($arProperty['CODE'] == 'photos' || $arProperty['CODE'] == 'labels' || $arProperty['CODE'] == 'services'){
						if($arProperty['CODE'] == 'labels' || $arProperty['CODE'] == 'services'){
							$arItemRes['params'][$arProperty['CODE']][] = process_fields($arProperty);
						}
						elseif($i == 0){
							$arItemRes['params'][$arProperty['CODE']][] = process_fields($arProperty);
							$i++; 
						}
					}
					else
						$arItemRes['params'][$arProperty['CODE']] = process_fields($arProperty);
				}
			}
			if(!$arItemRes['params']['photos']) 
				$arItemRes['params']['photos'][] = ['small' => DEFAULT_PRICTURE, 'mid' => DEFAULT_PRICTURE];
			if($arItemRes['category_id'] == BOOKING){
				//LogData("GetProducts", GetProducts(null, $arItemRes['category_id'], $arProperty, $arItem['IBLOCK_ID'], $arItem['IBLOCK_SECTION_ID'], $params));
				//LogData("GetProducts", [null, $arItemRes['category_id'], $arProperty, $arItem['IBLOCK_ID'], $arItem['IBLOCK_SECTION_ID'], $params]);


							//$products = GetProducts(null, $arItemRes['category_id'], $arProperty, $arItem['IBLOCK_ID'], $arItem['IBLOCK_SECTION_ID'], $params);

				$arItemRes['params']['price'] = GetHotelBestPrice($arItemRes['id'], $params);//GetPrice($arItemRes['id'], $params, $params['dateIn']);
				//LogData("Price", [$arItemRes['id'], $params, $arItemRes['name']]);
				if($arItemRes['params']['price']){
					$arItemRes['params']['canbuy'] = true;
				}
				else {
					$arItemRes['params']['canbuy'] = false;
				}
				
			}
			else{
				$arItemRes['params']['canbuy'] = false;
				$price = CPrice::GetBasePrice($arItemRes['id'])['PRICE'];
				if($price != "0.00" || !$price)
					$arItemRes['params']['price'] = CPrice::GetBasePrice($arItemRes['id'])['PRICE'];
			}
			
			$arItems['items'][] = $arItemRes;
		//return $arItems;
	}	
	//$Ent = new ENTITY($params['categoryId']);
	//if($_REQUEST[DBG] == 1)	{
	$count = $res->nSelectedCount;
	//$arItems['totalPages'] = $res->NavPageCount;
	$arItems['totalItems'] = (int)$res->NavRecordCount;
	//$arItems['currentPage'] = $res->NavNum;
	$arItems['offset'] = $params["offset"];
	
    return $arItems;
}


?>
