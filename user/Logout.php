<?php

function Logout($params){
    global $USER;

    if($USER->IsAuthorized()){
        $USER->Logout();
        return ['msg' => 'Выход выполнен', 'type' => 'logout'];
    }
    else{
        return ['msg' => 'вы не авторизованы ', 'type' => 'logout'];
    }
}

?>