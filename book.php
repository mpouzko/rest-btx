<?php
include_once $_SERVER['DOCUMENT_ROOT'] .'/rest/libs/simplehtmldom/simple_html_dom.php';

if(array_key_exists('url', $_REQUEST))
    $url = $_REQUEST['url'];
else
//$url = "https://www.booking.com/hotel/ru/mariott-krasnaya-polyana.ru.html?label=gen173nr-1FCAEoggI46AdIM1gEaMIBiAEBmAEhuAEXyAEM2AEB6AEB-AELiAIBqAIDuALP9fzrBcACAQ;sid=bcb0e3033a101465153979fa1ee72bbb;dest_id=-2906048;dest_type=city;dist=0;group_adults=2;group_children=0;hapos=1;hpos=1;no_rooms=1;room1=A%2CA;sb_price_type=total;sr_order=popularity;srepoch=1568619246;srpvid=5667353774fa011b;type=total;ucfs=1&#map_closed";
    $url = "https://www.booking.com/hotel/ru/mariott-krasnaya-polyana.ru.html";
//$url = "https://www.booking.com/hotel/ru/apartments-at-naberezhnaya-vremena-goda.ru.html";
echo $url;
//die();

$url = $url.'#tab-reviews';

$mainarray = array ();

//$hoteluid = explode("sid=", $url);
//$hoteluid = explode(";", $hoteluid[1]);
//$hoteluid = $hoteluid[0];

$result = ParseHotelData($url);
//echo "---2";
echo '<pre>';
print_r($result);
echo '</pre>';
//foreach ($result['Photos'] as ){

//}

function ParseHotelData($url){
    $main_array = array(
        'Title' =>"",
        'Logo' =>'',
        'booking_uid' => "",
        'geo-location' => array('lattitude'=>'','longitude'=>''),
        'important_facilitys' =>array(),
        'description' => '',
        'Photos' => array(),
        'Rating' => '',
    );
    $html = file_get_html($url);
	//print_r($html);
    //$html = str_get_html($html);
    $ret = $html->find('#basiclayout');
	//print_r($ret);
    $ret = $ret[0];
	//print_r($ret);
    $main_array['Title'] = getHotelTitle($ret);
    $main_array['description'] = getHotelDesc($ret);
    $main_array['Logo'] = getHotelLogo($ret); // Могут отсутствовать
    $main_array['Photos'] = getHotelPhotos($ret); // Могут отсутствовать
    $main_array['geo-location'] = getHotelGeoLocation($ret);
    $main_array['booking_uid'] = getHotelSID($ret);
    $main_array['important_facilitys'] = getHotelFacilities($ret);
    $main_array['Rating'] = getHotelRating($ret);
    $main_array['GuestsLike'] = getHotelGuestsLike($ret);
    $main_array['GuestsLikeHTML'] = getHotelGuestsLikeHTML($ret);
    //$main_array['facilitiesChecklists'] = facilitiesChecklist($ret);
    $main_array['TermsOfPlacement'] = getHotelTermsOfPlacement($ret);
    $main_array['CountRooms'] = getHotelRooms2($ret);
    $main_array['Rooms'] = getHotelRooms($ret);

    //return json_encode($main_array,true);
    return $main_array;
}

function getHotelGuestsLikeHTML($in_html){
    $retarr = array();
    $i=0;
    $html = str_get_html($in_html);
    $ret = $html->find('.ph-icon-fill-color', 0);
    //$retarr = $ret->innertext;
    //echo count($ret);
    //foreach($ret as $fac) {
    $h = $ret->innertext;
        $retarr[$i] = htmlspecialchars($h);
        $i++;
        $retarr[$i] = $h;
    //}
    return $retarr;
    //return count($ret);
}



function getHotelRooms($in_html){
    $retarr = array();
    $i=0;
    $x=0;
    $html = str_get_html($in_html);
    $ret = $html->find('#maxotel_rooms tbody',0)->children();
    //$retarr = $ret->innertext;
    //echo count($ret);
    foreach($ret as $fac) {
        if(($i % 2) == 0){
            $retarr[$x]['name'] = $fac->find('.room-info .togglelink',0)->innertext;
            $retarr[$x]['adults'] = count($fac->find('.occ_no_dates .occupancy_adults i'));
            $retarr[$x]['chilrens'] = count($fac->find('.occ_no_dates .occupancy_children'));

            //$retarr[$i] = getHotelRoomsData($fac->innertext);
            $x++;
        }
        $i++;
    }
    return $retarr;
    //return count($ret);
}

function getHotelRoomsData($in_html){
    $retarr = array();
    $i=0;
    $html = str_get_html($in_html);
    $ret = $html->find('#maxotel_rooms',0)->find('tbody', 0)->find('tr');//->find('!.extendedRow');
    return $retarr;
}

function getHotelRooms2($in_html){
    $retarr = array();
    $i=0;
    $html = str_get_html($in_html);
    $ret = $html->find('#maxotel_rooms',0)->find('tbody', 0)->find('tr');
    //$retarr = $ret->innertext;
    //echo count($ret);
    //foreach($ret as $fac) {
      //  $retarr[$i] = $fac->innertext;
        //$i++;
    //}
    //return $retarr;
    return count($ret);
}
//--не работает
function  getHotelTermsOfPlacement($in_html){
    $retarr = array();
    $i=0;
    $html = str_get_html($in_html);
    $ret = $html->find('#hp_policies_box');//->find('.facilitiesChecklistSection');
    //$retarr = $ret->innertext;
    //echo count($ret);
    //foreach($ret as $fac) {
    //  $retarr[$i] = $fac->innertext;
    //$i++;
    //}
    //return $retarr;
    return count($ret);
}
//terms of placement

//---Не работает
function facilitiesChecklist($in_html){
    $retarr = array();
    $i=0;
    $html = str_get_html($in_html);
    $ret = $html->find('.facilitiesChecklist');//->find('.facilitiesChecklistSection');
    //$retarr = $ret->innertext;
    //echo count($ret);
    //foreach($ret as $fac) {
      //  $retarr[$i] = $fac->innertext;
        //$i++;
    //}
    //return $retarr;
    return count($ret);

}

function getHotelGuestsLike($in_html)
{
    $retarr = array();
    $i=0;
    $html = str_get_html($in_html);
    $ret = $html->find('.ph-icon-fill-color', 0)->find('.ph-section');
    //$retarr = $ret->innertext;
    //echo count($ret);
    foreach($ret as $fac) {
        $retarr[$i] = $fac->innertext;
        $i++;
    }
    return $retarr;
    //return count($ret);

}
function getHotelRating($in_html)
{
    $retarr = array();
    $i=0;
    $html = str_get_html($in_html);
    $ret = $html->find('#review_list_score', 0)->find('li');
    //$retarr = $ret->innertext;
    //echo count($ret);
    foreach($ret as $fac) {
        //echo $fac->innertext;
        $html2 = str_get_html($fac->innertext);
        $rtt = $html2->find('p');
        $retarr[$i]['par'] = $rtt[0]->innertext;
        $retarr[$i]['val'] = $rtt[1]->innertext;
        $i++;
    }
    return $retarr;
}

function getHotelFacilities($in_html){
    $retarr = array();
    $i=0;
    $html = str_get_html($in_html);
    $ret = $html->find('.hp_desc_important_facilities',0)->find('.important_facility');
    //echo count($ret);
    foreach($ret as $fac) {
        //echo $fac->innertext;
        $retarr[$i] = $fac->innertext;
        $i++;
    }
    return $retarr;

}


function getHotelSID($in_html){
    $html = str_get_html($in_html);
    $res = $html->find('#top-book', 0)->find('[name=hotel_id]', 0)->outertext;
//    $maxrooms = $html->find('#top-book', 0)->find('[name=maxrooms]', 0)->outertext;//->find('input', 1);
//    $aid = $html->find('#top-book', 0)->find('[name=aid]', 0)->outertext;//->find('input', 1);
    $ret = parseHotelSID($res);
    return $ret;
}

function parseHotelSID($in_html){
    $html = explode('value="', $in_html);
    if(count($html) < 2)
        return "";
    $html = explode('"', $html[1]);
    if(count($html) < 2)
        return "";
    return $html[0];
}

function getHotelGeoLocation($in_html){
    $html = str_get_html($in_html);
    $res = $html->find('#hotel_header');
    $ret = parseGeoLocation($res[0]->outertext);
    //return $ret;
    return $ret;
}

function parseGeoLocation($in_html){
    $html = explode('data-atlas-latlng="', $in_html);
    if(count($html) < 2)
        return "";
    $html = explode('"', $html[1]);
    if(count($html) < 2)
        return "";
    $html = explode(',', $html[0]);
    if(count($html) < 2)
        return "";
    return array('lattitude' => $html[0], 'longitude' => $html[1]);
}
function getHotelPhotos($in_html){
    $returnArray = array();
    $i = 0;
    $html = str_get_html($in_html);
    //echo $html->innertext;
    $res = $html->find('.hp-gallery-top'); //Все галереи hp-gallery-slides
    $res = str_get_html($res[0]->innertext);
    //print_r($res[0]->innertext);
    $res = $res->find('img'); //Все галереи hp-gallery-slides
    //echo count($res);
    //$res = str_get_html($res[0]->innertext);
    //$res = $html->find('img');
    //echo count($res);
    if(count($res) == 0)
        return null;
    foreach($res as $img)
    {
        $res = parseImgUrl($img->outertext);
        if($res != "")
            $returnArray[$i] = array($res, parseImgDesc($img->outertext));
        $i++;
    }
    return $returnArray;
}

function parseImgDesc($in_html){
    $html = explode('alt="', $in_html);
    if(count($html) < 2)
        return "";
    $html = explode('"', $html[1]);
    if(count($html) < 2)
        return "";
    return $html[0];
}

function parseImgUrl($in_html){
    $html = explode('data-highres="', $in_html);
    if(count($html) < 2)
        return "";
    $html = explode('"', $html[1]);
    if(count($html) < 2)
        return "";
    return $html[0];
}

function getHotelLogo($in_html){
    $html = str_get_html($in_html);
    $ret = $html->find('.chain-content-logo');
    if(count($ret) == 0)
        return null;
    //$tmp = explode("</span>", $ret[0]->innertext);
    //$tmp = explode("</h2>", $tmp[1]);
    //print_r($tmp[0]);
    return $ret[0]->src;
}


function getHotelDesc($in_html){
    $html = str_get_html($in_html);
    $ret = $html->find('#property_description_content');
    //$tmp = explode("</span>", $ret[0]->innertext);
    //$tmp = explode("</h2>", $tmp[1]);
    //print_r($tmp[0]);
    return $ret[0]->innertext;

}


function getHotelTitle($in_html){
    $html = str_get_html($in_html);
    $ret = $html->find('.hp__hotel-name');
    $tmp = explode("</span>", $ret[0]->innertext);
    $tmp = explode("</h2>", $tmp[1]);
    //print_r($tmp[0]);
    return $tmp[0];
}



?>